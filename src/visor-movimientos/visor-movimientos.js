import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import {} from '@polymer/polymer/lib/elements/dom-repeat.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class VisorMovimientos extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          background-color: #FB247F;
        }

      </style>

      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <link href="estilos.css" rel="stylesheet" type="text/css" />

      <div class="container shadow-lg" style="background-color: grey; color: white; font-family: arial; margin-top: 10px; font-size: large;">
        </br>
        <div class="row">
          <div class="card-icon2">
            <img src="src/pagina-principal/imagenes/icons8-volver-24.png" on-click="generaEvento"></img>
          </div>
          <p id="resumen" class="col-4 text-left" style="margin-left: 20px"></p>
          <p id="sinmvtos" class="col-6 offset-3 text-left" style="margin-left: 20px" hidden></p>
          <p id="saldo" class="col-3 offset-4 text-right"></p>
        </div>
      </div>

      <div id="blockerMvtos" class="text-center" hidden>
        <span class="spinner-border" style="width: 6rem; height: 6rem; z-index: 100; margin-top: 25%" role="status">
      </div>

      <div id="cabecera" class="container container-cabecera shadow-lg" hidden>
        <div class="row">
          <div class="col-2 text-center">FECHA VALOR</div>
          <div class="col-2 text-center">FECHA MOVIMIENTO</div>
          <div class="col-3 offset-1 text-center">TIPO MOVIMIENTO</div>
          <div class="col-3 offset-1 text-center">IMPORTE</div>
        </div>
      </div>
<!--          <dom-repeat items="{{cuentas}}"> -->

      <div id="repeat">
        <template is="dom-repeat" items="{{mvtos}}">
          <div class="container container-cuentas shadow-lg">
            <div class="row">
              <div class="col-2 text-center">[[item.fecvalor]]</div>
              <div class="col-2 text-center">[[item.fecope]]</div>
              <div class="col-3 offset-1 text-center">[[item.tipmvto]]</div>
              <div class="col-2 offset-1 text-right">[[item.importe]]</div>
              <div id="toggle" class="dropdown-toggle text-right" on-click="clickMvto"></div>
            </div>
          </div>
          <div id="detmvto" class="container container-detcuenta" hidden>
            <div class="row">
              <div class="col-6 text-left">
                <span class="font-weight-bold">CTA ORIGEN:</span>
                <span> [[item.ibanorigen]]</span>
              </div>
              <div class="col-6 text-left">
                <span class="font-weight-bold">CTA DESTINO:</span>
                <span> [[item.ibandestino]]</span>
              </div>
            </div>
            </br>
            <div class="row">
              <div class="col-4 text-left">
                <span class="font-weight-bold">HORA DE ALTA:</span>
                <span> [[item.horaalta]]</span>
              </div>
              <div class="col-4 text-left">
                <span class="font-weight-bold">SALDO ANT.:</span>
                <span> [[item.saldoant]]</span>
              </div>
              <div class="col-4 text-left">
                <span class="font-weight-bold">SALDO POST.:</span>
                <span> [[item.saldopost]]</span>
              </div>
            </div>
            </br>
            <div class="row">
              <div class="col-12 text-left">
                <span class="font-weight-bold">OBSERVACIONES:</span>
                <span> [[item.notas]]</span>
              </div>
            </div>
          </div>
        </template>
      </div>

<!--          </dom-repeat> -->

      <iron-ajax
        id="getMvtos"
        url="http://localhost:3000/apitechu/secure/moves/{{idfolio}}"
        handle-as="json"
        on-response="showData"
        on-error="trataError"
      >
      </iron-ajax>

    `;
  }

  static get properties() {
    return {
      mvtos: {
        type: Array,
      }, idfolio: {
        type: Number
      }, idcuenta: {
        type: String
      }, saldo: {
        type: Number
      }, lanzador: {
        type: String,
        observer: "_refreshMvtos"
      }, abierto: {
        type: Boolean,
        value: false
      }, indiceAbierto: {
        type: Number
      }, elementoAbierto: {
        type: Object
      }, token: {
        type: String
      }
    };
  }

  _refreshMvtos(newValue, oldValue) {
     console.log("lanzando petición");

     this.$.blockerMvtos.hidden = false;
     this.$.cabecera.hidden = true;
     this.$.repeat.hidden = true;

     if (this.abierto){

       this.elementoAbierto.hidden = true;
       this.abierto = false;
       this.indiceAbierto = null;
       this.elementoAbierto = null;
     }

    this.$.getMvtos.headers.authorization = "Bearer " + this.token;
    this.$.getMvtos.generateRequest();

    this.$.sinmvtos.hidden = true;
  }

  clickMvto(e) {

    if (this.abierto && this.indiceAbierto == e.model.index){

      this.elementoAbierto.hidden = true;
      this.abierto = false;
      this.elementoAbierto = null;
      this.indiceAbierto = null;
    }else {
      if (this.abierto){

        this.elementoAbierto.hidden = true;
        this.abierto = false;
        this.elementoAbierto = null;
        this.indiceAbierto = null;
      }

      var elemento = this.shadowRoot.querySelectorAll("#detmvto")[e.model.index];
      elemento.hidden = false;
      this.abierto = true;

      this.elementoAbierto = elemento;
      this.indiceAbierto = e.model.index;
    }
  }

  showData(data) {
    console.log("showData");

    this.$.blockerMvtos.hidden = true;

    if (data.detail.response.length > 0){

      var saldopost = this.saldo;

      this.mvtos = [];

      for (var i = 0; i < data.detail.response.length; i++){

        var mvto = {};

        mvto.fecvalor = data.detail.response[i].fecvalor;
        mvto.fecope = data.detail.response[i].fecalta.substring(0,10);
        mvto.horaalta = data.detail.response[i].fecalta.substring(10,25);
        mvto.tipmvto = data.detail.response[i].tipmvto;

        if (mvto.tipmvto == "CARGO" || mvto.tipmvto == "TRANSFERENCIA EMITIDA"){

          var importe = data.detail.response[i].importe * -1;
        }else {

          var importe = data.detail.response[i].importe;
        }

        var saldoant = saldopost - importe;

        if (data.detail.response[i].ibanorigen){

          mvto.ibanorigen = data.detail.response[i].ibanorigen;
        }

        if (data.detail.response[i].ibandestino){

          mvto.ibandestino = data.detail.response[i].ibandestino;
        }

        mvto.importe = importe.toLocaleString('es-ES', { style: 'currency', currency: 'EUR' });
        mvto.saldoant = saldoant.toLocaleString('es-ES', { style: 'currency', currency: 'EUR' });
        mvto.saldopost = saldopost.toLocaleString('es-ES', { style: 'currency', currency: 'EUR' });
        mvto.notas = data.detail.response[i].notas;

        this.push("mvtos", mvto);

        saldopost = saldoant;
      }

      this.$.resumen.innerText = "Cuenta: " + this.idcuenta;
      this.$.saldo.innerText = "Saldo: " + this.saldo.toLocaleString('es-ES', { style: 'currency', currency: 'EUR' });
      this.$.cabecera.hidden = false;

      this.$.repeat.hidden = false;
    }else {
      this.$.resumen.innerText = "";
      this.$.saldo.innerText = "";
      this.$.sinmvtos.innerText = "Cuenta sin movimientos";
      this.$.sinmvtos.hidden = false;

      this.mvtos = [];
    }
  }

  trataError(error) {
    console.log("Hubo un error");
    console.log(error);
    console.log(error.detail.error.message);

    this.$.blockerMvtos.hidden = true;
    this.$.sinmvtos.innerText = "Error al recuperar los movimientos " + error.detail.error.message;

    this.mvtos = [];
  }

  generaEvento() {

    this.dispatchEvent(
      new CustomEvent(
        "eventmvtos",
        {
          detail: {
            "evento": "finMvtos",
          }
        }
      )
    )
  }

  formateaFecha(fechaEntrada) {

    var fechaTrabajo = fechaEntrada;
    var mesTrabajo = fechaTrabajo.getMonth() + 1;

    if  (mesTrabajo < 10){

      var mes = "0" + mesTrabajo.toString();
    }else {

      var mes = mesTrabajo.toString();
    }

    var diaTrabajo = fechaTrabajo.getDate()

    if  (diaTrabajo < 10){

      var dia = "0" + diaTrabajo.toString();
    }else {

      var dia = diaTrabajo.toString();
    }

    var fecha = fechaTrabajo.getFullYear().toString() + "-" + mes + "-" + dia;

    return fecha;
  }
}

window.customElements.define('visor-movimientos', VisorMovimientos);
