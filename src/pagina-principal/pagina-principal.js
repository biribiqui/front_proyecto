import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '../login-usuario/login-usuario.js';
import '../logout-usuario/logout-usuario.js';
import '../alta-usuario/alta-usuario.js';
import '../baja-usuario/baja-usuario.js';
import '../consulta-usuario/consulta-usuario.js';
import '../modifica-usuario/modifica-usuario.js';
import '../alta-cuenta/alta-cuenta.js';
import '../visor-cuentas/visor-cuentas.js';
import '../consulta-prestamo/consulta-prestamo.js';
import '../about/datos-about.js';
import "@polymer/iron-pages/iron-pages.js";

/**
 * @customElement
 * @polymer
 */
class PaginaPrincipal extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          background-color: #FB247F;

        }
      </style>

      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
      <link href="estilos.css" rel="stylesheet" type="text/css" />

      <div style="position: sticky; top:0; z-index: 45">
        <div class="card" id="top">
      		<div class="container-fluid">
      			<div class="row">
      				<div class="col-1 offset-2" id="bandera" on-click="volverPaginaPrincipal">
      					<img src="src/pagina-principal/imagenes/bandera-moderdonia-pequeña-2.jpg"></img>
      				</div>
      				<div class="col-9">
      					<div class="col-12">
      						<div class="row">
      							<div class="col-1 offset-3" id="logo" on-click="volverPaginaPrincipal">
      								<img src="src/pagina-principal/imagenes/logo.png">
      							</div>
      							<div class="col-5 offset-3 btn-inicio">
      								<button type="button" id="btnalta" class="btn btn-secondary btn-alb" on-click="altaUsuario">Registrarse</button>
      								<button type="button" id="btnlogin" class="btn btn-secondary btn-alb" on-click="logInlogOut">Log In</button>
                      <div class="dropdown">
     								    <button type="button" id="btnusuario" class="btn btn-secondary dropdown-toggle btn-alb" on-click="opcionesUsuario" hidden></button>
      								    <div id="dropdownUsuario" class="dropdown-content">
      									    <a on-click="consultaUsuario">Consultar</a>
      									    <a on-click="editarUsuario">Editar</a>
      									    <a on-click="bajaUsuario">Dar de baja</a>
      								    </div>
                      </div>
      							</div>
      						</div>
      					</div>
      					<div class="col-12">
      						<nav class="navbar navbar-expand-sm navbar-light">
      							<ul class="navbar-nav">
      								<li class="nav-item alb-item1">
      									<a class="nav-link">PARTICULARES</a>
      								</li>
      							</ul>
      							<ul class="navbar-nav">
      								<li class="nav-item alb-item1">
      									<a class="nav-link">BANCA PERSONAL</a>
      								</li>
      							</ul>
      							<ul class="navbar-nav">
      								<li class="nav-item alb-item1">
      									<a class="nav-link">BANCA PRIVADA</a>
      								</li>
      							</ul>
      							<ul class="navbar-nav">
      								<li class="nav-item alb-item1">
      									<a class="nav-link">AUTONOMOS</a>
      								</li>
      							</ul>
      							<ul class="navbar-nav">
      								<li class="nav-item alb-item1">
      									<a class="nav-link">EMPRESAS</a>
      								</li>
      							</ul>
      							<ul class="navbar-nav">
      								<li class="nav-item alb-item1">
      									<a class="nav-link">INSTITUCIONES</a>
      								</li>
      							</ul>
      						</nav>
      					</div>
      				</div>
      			</div>
      		</div>
      	</div>

      	<div class="card" id="menu">
      		<div class="container">
      			<div class="row">
      				<nav class="navbar navbar-expand navbar-dark offset-1">
      					<ul class="navbar-nav">
      						<div class="card card-icon">
      							<img src="src/pagina-principal/imagenes/icons8-casa-24.png" on-click="volverPaginaPrincipal">
      						</div>
      						<li class="nav-item alb-item2">
      							<a id="home" class="nav-link" on-click="volverPaginaPrincipal">Home</a>
      						</li>
      						<div class="card card-icon">
      							<img id="navCuentas" src="src/pagina-principal/imagenes/icons8-montón-de-dinero-24.png" on-click="opcionesCuentas">
      						</div>
                  <li class="nav-item dropdown alb-item2" on-click="opcionesCuentas">
                    <a id="navCuentas" class="nav-link dropdown-toggle" data-toggle="dropdown">Cuentas</a>
                      <div id="dropdownCuentas" class="dropdown-content">
                        <a class="nav-link" on-click="altaCuenta">Contratar Cuenta</a>
                        <a class="nav-link" on-click="consultaCuentas">Consultar Cuentas</a>
                    </div>
                  </li>
      						<div class="card card-icon">
      							<img src="src/pagina-principal/imagenes/icons8-tarjetas-bancarias-filled-24.png">
      						</div>
      						<li class="nav-item alb-item2">
      							<a class="nav-link">Tarjetas</a>
      						</li>
      						<div class="card card-icon">
      							<img id="navPtmos" src="src/pagina-principal/imagenes/icons8-hipoteca-filled-24.png" on-click="opcionesPtmos">
      						</div>
                  <li class="nav-item dropdown alb-item2" on-click="opcionesPtmos">
                    <a id="navPtmos" class="nav-link dropdown-toggle" data-toggle="dropdown">Hipotecas y Préstamos</a>
                      <div id="dropdownPtmos" class="dropdown-content">
                        <a class="nav-link" on-click="consultaPtmos">Calcule su Préstamo</a>
                    </div>
                  </li>
      						<li class="nav-item alb-item2">
      						</li>
      						<div class="card card-icon">
      							<img id="navAbout" src="src/pagina-principal/imagenes/icons8-acerca-de-24.png" on-click="opcionesAbout">
      						</div>
      						<li class="nav-item dropdown alb-item2" on-click="opcionesAbout">
      							<a id="navAbout" class="nav-link dropdown-toggle" data-toggle="dropdown">About</a>
      								<div id="dropdownAbout" class="dropdown-content">
      									<a class="nav-link" on-click="quienesSomos">Quienes Somos</a>
      									<a class="nav-link" on-click="dondeEstamos">Donde Estamos</a>
      									<a class="nav-link" on-click="elEquipo">Nuestro Equipo</a>
      								</div>
      						</li>
      					</ul>
      				</nav>
      			</div>
      		</div>
      	</div>
      </div>

      <div id="fondo" class="container">
		    <img class="img-fluid mx-auto shadow-lg" src="src/pagina-principal/imagenes/moderdonia2-930x600.jpg" width="100%" height="100%">
      </div>

      <iron-pages selected="[[componente]]" attr-for-selected="component-name">
        <div component-name="alta-usuario"><alta-usuario on-eventalta="procesaEvento"></alta-usuario></div>
        <div component-name="baja-usuario"><baja-usuario id="baja" on-eventbaja="procesaEvento"></baja-usuario></div>
        <div component-name="consulta-usuario"><consulta-usuario id="consulta" on-eventconsulta="procesaEvento"></consulta-usuario></div>
        <div component-name="modifica-usuario"><modifica-usuario id="modifica" on-eventmodifica="procesaEvento"></modifica-usuario></div>
        <div component-name="login-usuario"><login-usuario on-eventlogin="procesaEvento"></login-usuario></div>
        <div component-name="logout-usuario"><logout-usuario id="logout" on-eventlogout="procesaEvento"></logout-usuario></div>
        <div component-name="alta-cuenta"><alta-cuenta id="altacuenta" on-eventcuenta="procesaEvento"></alta-cuenta></div>
        <div component-name="visor-cuentas"><visor-cuentas id="cuentas"></visor-cuentas></div>
        <div component-name="consulta-prestamo"><consulta-prestamo id="ptmo" on-eventptmo="procesaEvento"></consulta-prestamo></div>
        <div component-name="datos-about"><datos-about id="about"></datos-about></div>
      </iron-pages>

    `;
  }

  static get properties() {
    return {
      componente: {
        type: String
      }, idUsuario: {
        type: Number
      }, email: {
        type: String
      }, datos: {
        type: Object
      }, logged: {
        type: Boolean,
        value: false
      }, token: {
        type: String
      }
    };
  }

  ready() {

    this.addEventListener("click", this._onClick, true);

    super.ready();
  }

  _onClick(event) {

    this._ocultaDropdowns(event);
  }

  _ocultaDropdowns(e) {

    if (!e.composedPath()[0].matches("#btnusuario")){

      if (this.$.dropdownUsuario.classList.contains("show")){

        this.$.dropdownUsuario.classList.remove('show');
      }
    }

    if (!e.composedPath()[0].matches("#navCuentas")){

        if (this.$.dropdownCuentas.classList.contains("show")){

        this.$.dropdownCuentas.classList.remove('show');
      }
    }

    if (!e.composedPath()[0].matches("#navPtmos")){

      if (this.$.dropdownPtmos.classList.contains("show")){

        this.$.dropdownPtmos.classList.remove('show');
      }
    }

    if (!e.composedPath()[0].matches("#navAbout")){

      if (this.$.dropdownAbout.classList.contains("show")){

        this.$.dropdownAbout.classList.remove('show');
      }
    }
  }

  opcionesUsuario() {

    this.$.dropdownUsuario.classList.toggle("show");
  }

  opcionesCuentas() {

   if (this.logged){

      this.$.dropdownCuentas.classList.toggle("show");
   }
  }

  opcionesPtmos() {

    this.$.dropdownPtmos.classList.toggle("show");
  }

  opcionesAbout() {

    this.$.dropdownAbout.classList.toggle("show");
  }

  bajaUsuario() {

    console.log("Baja usuario " + this.idUsuario + " " + this.email);

    this.$.baja.idUsuario = this.idUsuario;
    this.$.baja.email = this.email;
    this.$.baja.token = this.token;
    this.componente = "baja-usuario";
    this.$.fondo.hidden = false;
  }

  editarUsuario() {

    console.log("Editar usuario");

    this.$.modifica.idUsuario = this.idUsuario;
    this.$.modifica.email = this.email;
    this.$.modifica.datos = this.datos;
    this.$.modifica.token = this.token;

    this.componente = "modifica-usuario";

    this.$.fondo.hidden = false;
  }

  consultaUsuario() {

    console.log("Consulta usuario");

    this.$.consulta.datos = this.datos;
    this.$.consulta.lanzador = new Date();

    this.componente = "consulta-usuario";

    this.$.fondo.hidden = false;
  }

  volverPaginaPrincipal() {

    console.log("Home pulsado");

    this.$.fondo.hidden = false;

    this.componente = "pagina-principal";
  }

  logInlogOut() {

    if (!this.logged){

      this.componente = "login-usuario";
    }
    else {
      this.$.logout.idUsuario = this.idUsuario;
      this.$.logout.token = this.token;
      this.componente = "logout-usuario";
    }

    this.$.fondo.hidden = false;
  }

  altaUsuario() {

    this.componente = "alta-usuario";

    this.$.fondo.hidden = false;
  }

  altaCuenta() {

    console.log("Alta de cuenta");

    this.$.dropdownCuentas.classList.toggle("show");

    var fechaTrabajo = new Date();
    var mesTrabajo = fechaTrabajo.getMonth() + 1;

    if  (mesTrabajo < 10){

      var mes = "0" + mesTrabajo.toString();
    }else {

      var mes = mesTrabajo.toString();
    }

    var diaTrabajo = fechaTrabajo.getDate()

    if  (diaTrabajo < 10){

      var dia = "0" + diaTrabajo.toString();
    }else {

      var dia = diaTrabajo.toString();
    }

    var fecha = fechaTrabajo.getFullYear().toString() + "-" + mes + "-" + dia;

    this.$.altacuenta.idUsuario = this.idUsuario;
    this.$.altacuenta.fecalta = fecha;
    this.$.altacuenta.token = this.token;
    this.componente = "alta-cuenta";

    this.$.fondo.hidden = true;
  }

  consultaCuentas() {

    console.log("consulta cuentas");

    if (this.logged){

      this.$.dropdownCuentas.classList.toggle("show");

      this.$.cuentas.idUsuario = this.idUsuario;
      this.$.cuentas.nombre = this.$.btnusuario.textContent;
      this.$.cuentas.token = this.token;
      this.$.cuentas.lanzador = new Date();
      this.$.fondo.hidden = true;

      this.componente = "visor-cuentas";
    }
  }

  consultaPtmos() {

    this.$.dropdownPtmos.classList.toggle("show");
    this.$.fondo.hidden = true;

    this.$.ptmo.lanzador = new Date();
    this.componente = "consulta-prestamo";
  }

  quienesSomos() {

    console.log("Quienes somos");
    this.$.dropdownAbout.classList.toggle("show");
    this.$.fondo.hidden = true;
    this.$.about.opcion = "quien";
    this.componente = "datos-about";
  }

  dondeEstamos() {

    console.log("Donde estamos");
    this.$.dropdownAbout.classList.toggle("show");
    this.$.fondo.hidden = true;
    this.$.about.opcion = "donde";
    this.componente = "datos-about";
  }

  elEquipo() {

    console.log("El equipo");
    this.$.dropdownAbout.classList.toggle("show");
    this.$.fondo.hidden = true;
    this.$.about.opcion = "equipo";
    this.componente = "datos-about";
  }

  procesaEvento(e) {
    console.log("Capturado evento del emisor");
    console.log(e.detail.evento);

    switch (e.detail.evento)
		{
	    case "logIn":

        this.usuarioConectado(e.detail.idUsuario, e.detail.datos, e.detail.email);

        this.token = e.detail.token;

        break;

      case "logOut":

        this.usuarioDesconectado();

        break;

      case "altaUsuario":

        this.usuarioConectado(e.detail.idUsuario, e.detail.datos, e.detail.email);

        this.token = e.detail.token;

        break;

      case "modificaUsuario":

        this.usuarioConectado(e.detail.idUsuario, e.detail.datos, e.detail.email);

        break;

      case "bajaUsuario":

        this.usuarioDesconectado();

        break;

      default:

       this.componente="pagina-principal";
    }

    this.$.fondo.hidden = false;

    this.componente = "pagina-principal";
  }

  usuarioConectado(usuario, datos, email) {

    console.log("logando " + usuario + " " + datos.nombre);

    this.$.btnlogin.textContent = "Log Out";
    this.$.btnusuario.textContent = datos.nombre;
    this.$.btnusuario.hidden = false;
    this.$.btnalta.disabled = true;
    this.logged = true;
    this.idUsuario = usuario;
    this.email = email;
    this.datos = datos;
  }

  usuarioDesconectado() {

    this.$.btnlogin.textContent = "Log In";
    this.$.btnusuario.textContent = "";
    this.$.btnusuario.hidden = true;
    this.$.btnalta.disabled = false;

    this.logged = false;
    this.idUsuario = null;
  }
}

window.customElements.define('pagina-principal', PaginaPrincipal);
