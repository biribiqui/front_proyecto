import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '../alta-movimientos/alta-bbdd.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class AltaMovimientos extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }

      </style>

      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <link href="estilos.css" rel="stylesheet" type="text/css" />

      <div id="resumenAlta" class="container text-center shadow-lg" style="heigth: 80px">
        </br>
        <p>ALTA DE MOVIMIENTOS</p>
        <div class="row">
          <p class="col-4 offset-1 text-lg-left">Cuenta: [[idcuenta]]<p>
          <p class="col-6 text-right">Saldo: [[saldoedit]]<p>
        <div>
      </div>

      <div id="blocker" class="text-center" hidden>
        <span class="spinner-border" style="width: 6rem; height: 6rem; z-index: 100; margin-top: 25%" role="status">
      </div>

      <div class="container container-detcuenta shadow-lg">
        <form name="datosalta">
          <div class="form-row">
            <div class="form-group col-2">
              <label>Fecha de Valor:</label>
              <input id="fecvalor" type="date" class="form-control" on-change="validaDatos" autofocus>
            </div>
            <div class="form-group col-2">
              <label>Tipo de Movimiento:</label>
              <select id="tipmvto" class="form-control" on-change="validaTipoMVto">
                <option value="CARGO">Cargo</option>
                <option value="ABONO">Abono</option>
                <option value="TRANSFERENCIA">Transferencia</option>
              </select>
            </div>
            <div class="form-group col-1">
              <label>IBAN</label>
              <input id="ibandestino1" type="text" class="form-control" maxlength="4" on-change="validaDatos" disabled>
            </div>
            <div class="form-group col-1">
              <label style="color: #FFD2D2">IBAN</label>
              <input id="ibandestino2" type="text" class="form-control" maxlength="4" on-change="validaDatos" disabled>
            </div>
            <div class="form-group col-1">
              <label style="color: #FFD2D2">IBAN</label>
              <input id="ibandestino3" type="text" class="form-control" maxlength="4" on-change="validaDatos" disabled>
            </div>
            <div class="form-group col-1">
              <label style="color: #FFD2D2">IBAN</label>
              <input id="ibandestino4" type="text" class="form-control" maxlength="4" on-change="validaDatos" disabled>
            </div>
            <div class="form-group col-1">
              <label style="color: #FFD2D2">IBAN</label>
              <input id="ibandestino5" type="text" class="form-control" maxlength="4" on-change="validaDatos" disabled>
            </div>
            <div class="form-group col-1">
              <label style="color: #FFD2D2">IBAN</label>
              <input id="ibandestino6" type="text" class="form-control" maxlength="4" on-change="validaDatos" disabled>
            </div>
            <div class="form-group col-2">
              <label>Importe:</label>
              <input id="importe" type="text" class="form-control" maxlength="10" on-change="validaImporte">
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-12">
              <label>Observaciones:</label>
              <textarea class="form-control" rows="1" id="notas"></textarea>
            </div>
          </div>
        </form>
        <div id="cajaerror" class="alerterror">
          <span id="erroralta" style="color: #FFD2D2">ERROR:</span>
          </br>
        </div>
        </br>
        <div class="text-center">
          <button type="button" class="btn btn-secondary btn-alb" id="btnAlta" on-click="altaMvto" disabled>Aceptar</button>
          <button type="button" class="btn btn-secondary btn-alb" id="btnAltaCancel" on-click="cancelAlta">Cancelar</button>
        </div>
      </div>

      <alta-bbdd id="altaBBDD" on-eventbbdd="trataRespuestaAlta"></alta-bbdd>

      <iron-ajax
        id="getCuentaIban"
        url="http://localhost:3000/apitechu/secure/accounts/iban"
        handle-as="json"
        content-type="application/json"
        method="POST"
        on-response="trataRespuesta"
        on-error="trataAcceso"
      >
      </iron-ajax>
    `;
  }

  static get properties() {
    return {
      idUsuario: {
        type: Number
      }, idfolio: {
        type: Number
      }, idcuenta: {
        type: String
      }, iban: {
        type: String
      }, saldo: {
        type: Number
      }, limite: {
        type:Number
      }, saldoedit: {
        type: String
      }, token: {
        type: String
      }
    };
  }

  validaTipoMVto() {

      if (this.$.tipmvto.value == "TRANSFERENCIA"){

        this.$.ibandestino1.disabled = false;
        this.$.ibandestino2.disabled = false;
        this.$.ibandestino3.disabled = false;
        this.$.ibandestino4.disabled = false;
        this.$.ibandestino5.disabled = false;
        this.$.ibandestino6.disabled = false;
      }else {

        this.$.ibandestino1.disabled = true;
        this.$.ibandestino2.disabled = true;
        this.$.ibandestino3.disabled = true;
        this.$.ibandestino4.disabled = true;
        this.$.ibandestino5.disabled = true;
        this.$.ibandestino6.disabled = true;
      }
  }

  validaDatos() {

      var correcto = true;

      if (this.$.tipmvto == "TRANSFERENCIA" && (this.$.ibandestino1.length < 4 || this.$.ibandestino2.length < 4 ||
          this.$.ibandestino3.length < 4 || this.$.ibandestino4.length < 4 || this.$.ibandestino5.length < 4 ||
          this.$.ibandestino6.length < 4)){

          correcto = false;
      }

      if (this.$.fecvalor.value == ""){

        correcto = false;
      }

      if (this.$.importe.value.length == 0){

        correcto = false;
      }

      if (correcto){

        this.$.btnAlta.disabled = false;
      }else {

        this.$.btnAlta.disabled = true;
      }
  }

  validaImporte() {

    var correcto = true;

    var importe = Number(this.$.importe.value);

    if (isNaN(importe)){

      alert("ERROR: formate de importe incorrecto");

      this.$.importe.focus();

      correcto = false;
    }else {

      if (importe <= 0){

        alert("ERROR: el importe debe ser mayor que cero");

        this.$.importe.focus();

        correcto = false;
      }else {

        if (this.$.tipmvto.value != "ABONO"){

          importe = importe * -1;
        }

        var resultado = this.saldo + this.limite + importe;

        if (resultado < 0){

          alert("ERROR: saldo resultante + limite descubierto negativo");

          this.$.importe.focus();

          correcto = false;
        }
      }
    }

    this.validaDatos();

    return correcto;
  }

  cancelAlta() {

    this.inicializaDatos();

    this.generaEvento("cancelAlta");
  }

  altaMvto() {

    console.log("Lanzando alta");

    if (this.validaImporte()){

      if (this.$.tipmvto.value == "TRANSFERENCIA"){

        var consultaData = {
          "iban": this.$.ibandestino1.value + " " + this.$.ibandestino2.value + " " + this.$.ibandestino3.value + " " +
                  this.$.ibandestino4.value + " " + this.$.ibandestino5.value + " " + this.$.ibandestino6.value
        }

        this.$.blocker.hidden = false;

        this.$.getCuentaIban.headers.authorization = "Bearer " + this.token;
        this.$.getCuentaIban.body = JSON.stringify(consultaData);
        this.$.getCuentaIban.generateRequest();
      }else {

        var altaData = {
          "idUsuario":this.idUsuario,
          "idfolio":this.idfolio,
          "fecvalor":this.$.fecvalor.value,
          "tipmvto":this.$.tipmvto.value,
          "importe":Number(this.$.importe.value),
          "saldo":this.saldo,
          "notas":this.$.notas.value
        }

        this.$.blocker.hidden = false;

        this.$.altaBBDD.datos = altaData;
        this.$.altaBBDD.lanzador = new Date();
        this.$.altaBBDD.token = this.token;
      }
    }
  }

  trataRespuestaAlta(e) {
    console.log("respuesta alta " + e.detail.msg + " " + e.detail.idfolio);

    if (e.detail.msg == "Mvto creado"){

      if (this.$.tipmvto.value == "TRANSFERENCIA" && e.detail.idfolio != this.idfolio){

        var altaData = {
          "idUsuario":this.idUsuario,
          "idfolio":this.idfolio,
          "fecvalor":this.$.fecvalor.value,
          "tipmvto":"TRANSFERENCIA EMITIDA",
          "ibandestino":this.$.ibandestino1.value + " " + this.$.ibandestino2.value + " " + this.$.ibandestino3.value + " " +
                        this.$.ibandestino4.value + " " + this.$.ibandestino5.value + " " + this.$.ibandestino6.value,
          "importe":Number(this.$.importe.value),
          "saldo":this.saldo,
          "notas":this.$.notas.value
        }

        this.$.altaBBDD.datos = altaData;
        this.$.altaBBDD.lanzador = new Date();
      }else {

        this.$.blocker.hidden = true;

        this.inicializaDatos();
        this.generaEvento("altaMvto");
      }
    }else {

      this.mensajeError(e.detail.msg);
    }
  }

  generaEvento(evento) {

    console.log("Genera evento " + evento);

    this.dispatchEvent(
      new CustomEvent(
        "eventalta",
        {
          detail: {
            "evento":evento,
            "tipmvto":this.$.tipmvto,
            "importe":Number(this.$.importe.value)
          }
        }
      )
    )
  }

  trataRespuesta(data){

    console.log("Respuesta consulta IBAN");

    if (data.detail.response.length == 1){

      var altaData = {
        "idUsuario":data.detail.response[0].idusuario,
        "idfolio":data.detail.response[0].idfolio,
        "fecvalor":this.$.fecvalor.value,
        "tipmvto":"TRANSFERENCIA RECIBIDA",
        "ibanorigen":this.iban,
        "importe":Number(this.$.importe.value),
        "saldo":data.detail.response[0].saldo,
        "notas":this.$.notas.value
      }

      this.$.altaBBDD.datos = altaData;
      this.$.altaBBDD.lanzador = new Date();
    }else {
      if (data.detail.response.legth == 0){

        this.mensajeError("Cuenta destino no existe");
      }else {

        this.mensajeError("Cuenta destino duplicada");
      }
    }
  }

  mensajeError(mensaje) {

    this.$.blocker.hidden = true;
    this.$.erroralta.innerText = "ERROR: " + mensaje;
    this.$.erroralta.style.color = "black";

  }

  trataError(error) {
    console.log("Hubo un error");
    console.log(error);
    console.log(error.detail.error.message);

    this.mensajeError(error);
  }

  inicializaDatos() {

    this.$.tipmvto.value = "CARGO";
    this.$.fecvalor.value = "";
    this.$.importe.value = "";
    this.$.ibandestino1.value = "";
    this.$.ibandestino2.value = "";
    this.$.ibandestino3.value = "";
    this.$.ibandestino4.value = "";
    this.$.ibandestino5.value = "";
    this.$.ibandestino6.value = "";
    this.$.notas.value = "";

    this.$.btnAlta.disabled = true;
    this.$.fecvalor.focus();
    this.$.erroralta.style.color = "#FFD2D2";
  }
}

window.customElements.define('alta-movimientos', AltaMovimientos);
