import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class AltaBBDD extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }

      </style>

      <iron-ajax
        id="createMvto"
        url="http://localhost:3000/apitechu/secure/moves/alta"
        handle-as="json"
        content-type="application/json"
        method="POST"
        on-response="trataAlta"
        on-error="trataErrorAlta"
      >
      </iron-ajax>

      <iron-ajax
        id="updateAccounts"
        url="http://localhost:3000/apitechu/secure/accounts/update/{{datos.idUsuario}}"
        handle-as="json"
        content-type="application/json"
        method="POST"
        on-response="trataUpdate"
        on-error="trataErrorUpdate"
      >
      </iron-ajax>
    `;
  }

  static get properties() {
    return {
      datos: {
        type: Object
      }, lanzador: {
        type: Date,
        observer: "_lanzaPeticion"
      }, token: {
        type: String
      }
    };
  }

  _lanzaPeticion() {

    var datosAlta = {
      "idfolio": this.datos.idfolio,
      "fecvalor":this.datos.fecvalor,
      "tipmvto":this.datos.tipmvto,
      "importe":this.datos.importe,
      "ibanorigen":this.datos.ibanorigen,
      "ibandestino":this.datos.ibandestino,
      "notas":this.datos.notas
     }

     this.$.createMvto.headers.authorization = "Bearer " + this.token;
     this.$.createMvto.body = JSON.stringify(datosAlta);
     this.$.createMvto.generateRequest();
  }

  trataAlta(data){

    if (data.detail.response.msg == "Movimiento Creado") {

      if (this.datos.tipmvto == "ABONO" || this.datos.tipmvto == "TRANSFERENCIA RECIBIDA"){

        var importe = this.datos.importe;
      }else {

        var importe = this.datos.importe * -1;
      }

      var datosUpdate = {
        "operacion":"saldo",
        "idfolio":this.datos.idfolio,
        "saldo":Math.round((this.datos.saldo + importe) * 100) / 100,
       }

       this.$.updateAccounts.headers.authorization = "Bearer " + this.token;
       this.$.updateAccounts.body = JSON.stringify(datosUpdate);
       this.$.updateAccounts.generateRequest();
    }
    else {

      this.generaEvento(data.detail.response.msg);
    }
  }

  trataUpdate(data){

    console.log("data mensaje " + data.detail.response.msg);
    if (data.detail.response.msg == "Modificación correcta") {

      this.generaEvento("Mvto creado");
    }
    else {

      this.generaEvento("Error en alta de movimiento");
    }
  }

  trataErrorAlta(error) {

    this.generaEvento(error);
  }

  trataErrorUpdate(error) {

    this.generaEvento(error);
  }

  generaEvento(mensaje) {

    console.log("generando evento de vuelta");
    this.dispatchEvent(
      new CustomEvent(
        "eventbbdd",
        {
          detail: {
            "msg": mensaje,
            "idfolio":this.datos.idfolio
          }
        }
      )
    )
  }
}

window.customElements.define('alta-bbdd', AltaBBDD);
