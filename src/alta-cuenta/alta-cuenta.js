import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class AltaCuenta extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          background-color: #F0507F;
        }
      </style>

      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
      <link href="estilos.css" rel="stylesheet" type="text/css" />

      <div id="nuevaCuenta" class="container container-alta shadow-lg">
        </br>
        <h2 class="text-center" style="font-weight: bold">Alta de Cuenta</h2>
  			</br>
        <p class="offset-1">Por favor rellene los datos:</p>
        </br>
  			<form name="datosalta">
          <div class="form-row">
            <div class="form-group offset-1">
              <label>Oficina de la Cuenta:</label>
              <select id="oficina" class="form-control" autofocus>
                <option value="1133">1133 - Moderdonia Central</option>
                <option value="1134">1134 - Territorio Conquistado</option>
              </select>
            </div>
            <div class="form-group offset-1">
              <label>Fecha de Alta:</label>
      				<input id="fecalta" type="date" class="form-control" value="{{fecalta::input}}" disabled>
    				</div>
          </div>
          <div class="form-row">
            <div class="form-group offset-1">
              <label>Producto:</label>
              <select id="producto" class="form-control">
                <option value="CUENTA AHORRO">Cuenta Ahorro</option>
                <option value="CUENTA CORRIENTE">Cuenta Corriente</option>
                <option value="CUENTA VISTA">Cuenta Vista</option>
                <option value="CUENTA VIVIENDA">Cuenta Vivienda</option>
              </select>
            </div>
            <div class="form-group offset-1">
              <label>Límite Descubierto:</label>
      				<input id="limite" type="text" class="form-control" maxlength="10" on-change="validaDatos">
    				</div>
          </div>
          <div class="form-row">
            <div class="form-check form-check-inline offset-1">
              <input class="form-check-input" type="radio" id="noblq" value="noblq" on-click="trataCheck" checked>
              <label class="form-check-label" for="noblq">Sin Bloqueo</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" id="blqcargo" value="blqcargo" on-click="trataCheck">
              <label class="form-check-label" for="blqcargo">Bloqueo Cargos</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" id="blqabono" value="blqabono" on-click="trataCheck">
              <label class="form-check-label" for="inlineRadio3">Bloqueo Abonos</label>
            </div>
          </div>
          </br>
          <div class="form-row">
            <div class="form-group offset-1">
              <label>Saldo Inicial:</label>
      				<input id="saldo" type="text" class="form-control" maxlength="10" on-change="validaDatos">
    				</div>
            <div class="form-group offset-1">
              <label>Tipo Intervención:</label>
              <select id="intervencion" class="form-control">
                <option value="1er TITULAR">1er Titular</option>
                <option value="2do TITULAR">2do Titular</option>
                <option value="REPRESENTANTE">Representante</option>
                <option value="APODERADO">Apoderado</option>
              </select>
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-10 offset-1">
              <label>Observaciones:</label>
              <textarea class="form-control" rows="3" id="notas"></textarea>
    				</div>
          </div>
  			</form>
    		</br>
        <div id="cajaerror" class="alerterror">
            <span id="erroralta" style="color: #FFB4B4">ERROR:</span>
            </br>
        </div>
  			</br>
        </br>
        <div class="text-center">
        		<button type="button" class="btn btn-secondary btn-alb" id="btnAlta" on-click="altaCuenta" disabled>Aceptar
              <span id="spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true" hidden></span>
            </button>
        		<button type="button" class="btn btn-secondary btn-alb" id="btnAltaCancel" on-click="cancelAlta">Cancelar</button>
        </div>
      </div>

      <iron-ajax
        id="doAltaCuenta"
        url="http://localhost:3000/apitechu/secure/accounts/alta/"
        handle-as="json"
        content-type="application/json"
        method="POST"
        on-response="tratarAltaCuenta"
        on-error="tratarErrorCuenta"
      >
      </iron-ajax>

      <iron-ajax
        id="doAltaMvto"
        url="http://localhost:3000/apitechu/secure/moves/alta/"
        handle-as="json"
        content-type="application/json"
        method="POST"
        on-response="tratarAltaMvto"
        on-error="tratarErrorMvto"
      >
      </iron-ajax>

    `;
  }

  static get properties() {
    return {
      idUsuario: {
        type: Number
      }, fecalta: {
        type: Date
      }, token: {
        type: String
      }
    };
  }

  altaCuenta() {
    console.log("Aceptar pulsado");

    switch (this.$.producto.value)
    {
      case "CUENTA AHORRO":

        var contr = '0010';

        break;

      case "CUENTA CORRIENTE":

        var contr = '0020';

        break;

      case "CUENTA VISTA":

        var contr = '0030';

        break;

      case "CUENTA VIVIENDA":

        var contr = '0040';

        break;
    }

    if (this.$.noblq.checked){

      var blqcargo = "NO";
      var blqabono = "NO";
    }else {

      if (this.$.blqcargo.checked){

        var blqcargo = "SI";
        var blqabono = "NO";
      }else {

        var blqcargo = "NO";
        var blqabono = "SI";
      }
    }

    var altaData = {
    	"idUsuario":this.idUsuario,
    	"banco":"0182",
    	"oficina":this.$.oficina.value,
      "contr":contr,
      "producto":this.$.producto.value,
      "saldo":Number(this.$.saldo.value),
      "limite":Number(this.$.limite.value),
    	"intervencion":this.$.intervencion.value,
    	"estado":"ACTIVA",
    	"blqcargo":blqcargo,
    	"blqabono":blqabono,
    	"fecalta":this.fecalta,
      "notas":this.$.notas.value
    }

    this.$.doAltaCuenta.headers.authorization = "Bearer " + this.token;
    this.$.doAltaCuenta.body = JSON.stringify(altaData);
    this.$.doAltaCuenta.generateRequest();

    this.$.spinner.hidden = false;
  }

  trataCheck(e) {

    console.log("Tratando check");

    if (e.target.id == "noblq"){

      this.$.blqcargo.checked = false;
      this.$.blqabono.checked = false;
    }else {

      if (e.target.id == "blqcargo"){

        this.$.noblq.checked = false;
        this.$.blqabono.checked = false;
      }else {

        this.$.noblq.checked = false;
        this.$.blqcargo.checked = false;
      }
    }
  }

  cancelAlta() {
    console.log("Cancelar pulsado");

    this.generaEvento("cancelAltaCuenta");

    this.inicializa();
  }

  tratarAltaCuenta(data) {

    if (data.detail.response.msg == "Cuenta Creada") {

      if (Number(this.$.saldo.value) != 0){

        var altaData = {
        	"idfolio":data.detail.response.idfolio,
          "fecvalor":this.fecalta,
          "tipmvto":"APERTURA",
          "importe":Number(this.$.saldo.value)
        }

        this.$.doAltaMvto.headers.authorization = this.token;
        this.$.doAltaMvto.body = JSON.stringify(altaData);
        this.$.doAltaMvto.generateRequest();
      }else {

        this.generaEvento("altaCuenta");

        this.inicializa();
        this.$.spinner.hidden = true;
      }
    }
    else {

      this.$.erroralta.textContent = data.detail.response.msg;
      this.$.cajaerror.style.color = "black";
      this.$.spinner.hidden = true;
    }
  }

  tratarErrorCuenta(error) {

    if (error.detail.request.status == 404){

      this.$.erroralta.textContent = "No fue posible establecer la conexión";
    }
    else {
      this.$.erroralta.textContent = error.detail.error.message;
    }

    this.$.spinner. hidden = true;
    this.$.cajaerror.style.color = "black";
  }

  tratarAltaMvto(data) {

    if (data.detail.response.msg == "Movimiento Creado"){

      this.generaEvento("altaCuenta");

      this.inicializa();
      this.$.spinner.hidden = true;
    }
    else {

      this.$.erroralta.textContent = data.detail.response.msg;
      this.$.cajaerror.style.color = "black";
      this.$.spinner.hidden = true;
    }
  }

  tratarErrorMvto(error) {

    if (error.detail.request.status == 404){

      this.$.erroralta.textContent = "No fue posible establecer la conexión";
    }
    else {
      this.$.erroralta.textContent = error.detail.error.message;
    }

    this.$.spinner.hidden = true;
    this.$.cajaerror.style.color = "black";
  }

  generaEvento(evento) {

    this.dispatchEvent(
      new CustomEvent(
        "eventcuenta",
        {
          detail: {
            "evento": evento,
            "idUsuario" : this.idUsuario,
          }
        }
      )
    )
  }

  validaDatos() {

    var limite = this.$.limite;
    var saldo = this.$.saldo;
    var correcto = true;

    if (isNaN(Number(limite.value))){

      alert("Error en el limite descubierto: formato no numérico");

      limite.focus();

      correcto = false;
    }else {

      if (Number(limite.value) < 0){

        alert("Error en el limite descubierto: debe ser positivo");

        limite.focus();

        correcto = false;
      }
    }

    if (correcto){

      if (isNaN(Number(saldo.value))){

        alert("Error en el saldo inicial: formato no numérico");

        saldo.focus();

        correcto = false;
      }else {

        if (Number(saldo.value) < 0){

          alert("Error en el saldo inicial: debe ser positivo");

          saldo.focus();

          correcto = false;
        }
      }
    }

    if (correcto && !this.campoVacio(saldo.value) && !this.campoVacio(saldo.value)){

      console.log("Datos informados");

      this.$.btnAlta.disabled = false;
    }
  }

  campoVacio(datoEntrada) {

      var retorno = true;

      if (datoEntrada.length > 0 && datoEntrada != "")
      {
        for (var i = 0; i < datoEntrada.length; i++)
        {
          if (datoEntrada[i] != " ")
          {
            retorno = false;
          }
        }
      }

      return retorno;
  }

  inicializa() {

    this.$.cajaerror.style.color = "#FFB4B4";
    this.$.btnAlta.disabled = true;
    this.$.oficina.focus();
    this.$.oficina.value = "1133";
    this.$.producto.value = "CUENTA AHORRO";
    this.$.limite.value = "";
    this.$.noblq.checked = true;
    this.$.blqcargo.checked = false;
    this.$.blqabono.checked = false;
    this.$.saldo.value = "";
    this.$.intervencion.value = "1er TITULAR";
    this.$.notas.value = "";
  }
}

window.customElements.define('alta-cuenta', AltaCuenta);
