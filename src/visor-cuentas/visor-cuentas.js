import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import {} from '@polymer/polymer/lib/elements/dom-repeat.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '../visor-movimientos/visor-movimientos.js';
import '../alta-movimientos/alta-movimientos.js';

/**
 * @customElement
 * @polymer
 */
class VisorCuentas extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          background-color: #FB247F;
        }

      </style>

      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <link href="estilos.css" rel="stylesheet" type="text/css" />

      <div class="container-fluid shadow-lg" style="background-color: #FFB4B4; font-family: arial;">
        </br>
        <h2 id="saludo" class="text-center"></h2>
        </br>
      </div>

      <alta-movimientos id="altamvtos" on-eventalta="eventoAlta" hidden></alta-movimientos>

      <div id="resumen" class="container shadow-lg" style="background-color: grey; color: white; font-family: arial; margin-top: 10px; font-size: large;">
        </br>
        <div class="row">
          <p id="mensaje" class="col-2 text-left" style="margin-left: 20px"></p>
          <p id="sincuentas" class="col-6 offset-3 text-center" style="margin-left: 20px" hidden></p>
          <p id="saldo" class="col-4 offset-5 text-right"></p>
        </div>
      </div>

      <div id="blockerCuentas" class="text-center">
        <span class="spinner-border" style="width: 6rem; height: 6rem; z-index: 100; margin-top: 25%" role="status">
      </div>

      <div id="blockerCambios" class="text-center" hidden>
      </div>

      <div id="cabecera" class="container container-cabecera shadow-lg" hidden>
        <div class="row">
          <div class="col-3 offset-1">NUMERO DE CUENTA</div>
          <div class="col-1 offset-2 text-center">SITUACION</div>
          <div class="col-2 offset-3">SALDO</div>
        </div>
      </div>
<!--          <dom-repeat items="{{cuentas}}"> -->

      <div id="repeat">
        <template is="dom-repeat" items="{{cuentas}}">
          <div class="container container-cuentas shadow-lg">
            <div class="row">
              <div idfolio="[[item.idfolio]]" class="col-4 text-center">[[item.idcuenta]]</div>
              <div class="col-1 offset-2">[[item.estado]]</div>
              <div class="col-2 offset-2 text-right">[[item.saldoedit]]</div>
              <div id="toggle" class="dropdown-toggle text-right" on-click="clickCuenta"></div>
            </div>
          </div>
          <div id="detcuenta" class="container container-detcuenta" hidden>
            <div class="row">
              <div class="col-3 text-left">
                <span class="font-weight-bold">FECHA DE ALTA:</span>
                <span> [[item.fecalta]]</span>
              </div>
              <div class="col-4 text-left">
                <span class="font-weight-bold">PRODUCTO:</span>
                <span> [[item.producto]]</span>
              </div>
              <div class="col-5 text-left">
                <span class="font-weight-bold">COD. IBAN:</span>
                <span> [[item.iban]]</span>
              </div>
            </div>
            </br>
            <div class="row">
              <div class="form-inline col-3 text-left">
                <span class="font-weight-bold">FECHA CANCEL.:</span>
                <span> [[item.fecancel]]</span>
              </div>
              <div class="form-inline col-4 text-left" style="z-index: 60">
                <label class="font-weight-bold">LIMITE DESCUBIERTO:</label>
                <input id="limite" type="text" class="col-4 form-control-plaintext" value="{{item.limite::input}}" disabled>
              </div>
              <div class="form-inline col-4 text-left">
                <span class="font-weight-bold">TIPO DE INTERVENCION:</span>
                <span> [[item.intervencion]]</span>
              </div>
            </div>
            </br>
            </br>
            <div class="row">
              <button type="button" class="btn btn-secondary btn-detalle col-2 offset-1 mx-auto" on-click="clickMovtos"> Ver Movimientos</button>
              <button id="operar" type="button" class="btn btn-secondary btn-detalle col-2 offset-1 mx-auto" style="z-index: 60" on-click="clickOperar">Operar</button>
              <button type="button" class="btn btn-secondary btn-detalle col-2 offset-1 mx-auto" style="z-index: 60" on-click="clickModificar">Modificar Limite
                <span id="spinneractualiza" class="spinner-border spinner-border-sm" role="status" aria-hidden="true" hidden></span>
              </button>
              <button type="button" class="btn btn-secondary btn-detalle col-2 offset-1 mx-auto" on-click="clickAnular">Cancelar Cuenta
                <span id="spinnercancela" class="spinner-border spinner-border-sm" role="status" aria-hidden="true" hidden></span>
              </button>
            </div>
          </div>
        </template>
      </div>

      <visor-movimientos id="mvtos" on-eventmvtos="procesaEvento" hidden></visor-movimientos>
<!--          </dom-repeat> -->

      <iron-ajax
        id="getAccounts"
        url="http://localhost:3000/apitechu/secure/accounts/{{idUsuario}}"
        handle-as="json"
        on-response="showData"
        on-error="trataError"
      >
      </iron-ajax>

      <iron-ajax
        id="updateAccounts"
        url="http://localhost:3000/apitechu/secure/accounts/update/{{idUsuario}}"
        handle-as="json"
        content-type="application/json"
        method="POST"
        on-response="finUpdate"
        on-error="trataErrorUpdate"
      >
      </iron-ajax>
    `;
  }

  static get properties() {
    return {
      cuentas: {
        type: Array
      }, idUsuario: {
        type: Number
      }, lanzador: {
        type: String,
        observer: "_refreshCuentas"
      }, nombre: {
        type: String
      }, abierto: {
        type: Boolean,
        value: false
      }, indiceAbierto: {
        type: Number
      }, elementoAbierto: {
        type: Object
      }, limite: {
        type: Number
      }, indice: {
        type:Number
      }, cambiolimite: {
        type: Boolean,
        value: false
      }, token: {
        type: String
      }
    };
  }

  _refreshCuentas(newValue, oldValue) {
    console.log("lanzando petición");

    var hora = this.lanzador.getHours();

    if (hora >= 6 && hora < 12){

     var saludo = "¡Buenos días ";
    }else {
     if (hora >= 12 && hora < 22){

       var saludo = "¡Buenas tardes ";
     }else {

       var saludo = "¡Buenas noches ";
     }
    }

    this.$.saludo.innerText = saludo + this.nombre + "!";
    this.$.blockerCuentas.hidden = false;
    this.$.cabecera.hidden = true;
    this.$.repeat.hidden = true;
    this.$.sincuentas.hidden = true;
    this.$.mvtos.hidden = true;

    if (this.elementoAbiertoAlta){

     this.elementoAbiertoAlta.hidden = true;
   }

     if (this.abierto){

       this.elementoAbierto.hidden = true;
       this.abierto = false;
       this.indiceAbierto = null;
       this.elementoAbierto = null;
     }

    this.$.getAccounts.headers.authorization = "Bearer " + this.token;
    this.$.getAccounts.generateRequest();
  }

  clickCuenta(e) {

    if (this.abierto && this.indiceAbierto == e.model.index){

      this.elementoAbierto.hidden = true;
      this.abierto = false;
      this.elementoAbierto = null;
      this.indiceAbierto = null;
    }else {
      if (this.abierto){

        this.elementoAbierto.hidden = true;
        this.abierto = false;
        this.elementoAbierto = null;
        this.indiceAbierto = null;
      }

      var elemento = this.shadowRoot.querySelectorAll("#detcuenta")[e.model.index];
      elemento.hidden = false;
      this.abierto = true;

      this.elementoAbierto = elemento;
      this.indiceAbierto = e.model.index;
    }
  }

  clickMovtos(e) {

    console.log("click movimientos");

    this.$.resumen.hidden = true;
    this.$.cabecera.hidden = true;
    this.$.repeat.hidden = true;
    this.$.mvtos.idfolio = e.model.item.idfolio;
    this.$.mvtos.idcuenta = e.model.item.idcuenta;
    this.$.mvtos.saldo = e.model.item.saldo;
    this.$.mvtos.token = this.token;
    this.$.mvtos.hidden = false;
    this.$.mvtos.lanzador = new Date();
  }

  clickModificar(e) {

    console.log("click modificar");

    var error = false;

    var elemento = this.shadowRoot.querySelectorAll("#limite")[e.model.index];

    if (this.cambiolimite){

      if (isNaN(Number(elemento.value))){

        alert("Error en el importe: formato no numérico");

        error = true;
      }else {

        if (Number(elemento.value) + e.model.item.saldo < 0){

          alert("Error en el importe: no puede ser inferior al limite descubierto actual");

          error = true;
        }
      }

      if (!error){

        e.model.item.limite = Number(elemento.value);
        this.indice = e.model.index;

        e.target.children[0].hidden = false;

        this.lanzaUpdate("limite", e.model);
      }else {

        var elemento = this.shadowRoot.querySelectorAll("#limite")[e.model.index];

        elemento.focus();
      }
    }else {

      if (e.model.item.estado == "CANCELADA"){

        alert("Modificación no permitida, cuenta cancelada");
      }else {

        this.limite = e.model.item.limite;

        this.$.blockerCambios.hidden = false;

        elemento = this.shadowRoot.querySelectorAll("#operar")[e.model.index];
        elemento.textContent = "Cancelar Cambio"

        this.cambiolimite = true;

        var elemento = this.shadowRoot.querySelectorAll("#limite")[e.model.index];
        elemento.disabled = false;
        elemento.focus();
      }
    }
  }

  clickAnular(e) {

    console.log("click cancelar");

    if (e.model.item.estado == "CANCELADA"){

      alert("Modificación no permitida, cuenta ya cancelada");
    }else {

      if (Number(e.model.item.saldo) != 0){

        alert("Modificación no permitida, cuenta con saldo");
      }else {

        if (confirm("Se va a cancelar la cuenta seleccionada")){

          e.model.item.estado = "CANCELADA";

          var fechaTrabajo = new Date();
          var mesTrabajo = fechaTrabajo.getMonth() + 1;

          if  (mesTrabajo < 10){

            var mes = "0" + mesTrabajo.toString();
          }else {

            var mes = mesTrabajo.toString();
          }

          var diaTrabajo = fechaTrabajo.getDate()

          if  (diaTrabajo < 10){

            var dia = "0" + diaTrabajo.toString();
          }else {

            var dia = diaTrabajo.toString();
          }

          var fecha = fechaTrabajo.getFullYear().toString() + "-" + mes + "-" + dia;

          e.model.item.fecancel = fecha;

          this.indice = e.model.index;

          e.target.children[0].hidden = false;

          this.lanzaUpdate("cancelacion", e.model);
        }
      }
    }
  }

  showData(data) {
    console.log("showData " + data.detail.response.msg);

    this.$.blockerCuentas.hidden = true;

    if (data.detail.response.length > 0){

      this.cuentas = data.detail.response;

      var saldo = 0;

      for (var i = 0; i < this.cuentas.length; i++){

        saldo += this.cuentas[i].saldo;

        this.cuentas[i].saldoedit = this.cuentas[i].saldo.toLocaleString('es-ES', { style: 'currency', currency: 'EUR' });
      }

      this.$.mensaje.innerText = "Cuentas personales"
      this.$.saldo.innerText = "Saldo acumulado: " + saldo.toLocaleString('es-ES', { style: 'currency', currency: 'EUR' });
      this.$.cabecera.hidden = false;
      this.$.repeat.hidden = false;
    }else {
      this.$.mensaje.innerText = "";
      this.$.saldo.innerText = "";
      this.$.sincuentas.innerText = "Aun no tiene contratada ninguna cuenta";
      this.$.sincuentas.hidden = false;
    }
  }

  trataError(error) {
    console.log("Hubo un error");
    console.log(error);
    console.log(error.detail.error.message);

    this.$.blockerCuentas.hidden = true;

    this.cuentas = [];
  }

  lanzaUpdate(cambio, data) {

    console.log("Lanzando actualización ");

    var modificaData = {
      "operacion": cambio,
      "idfolio": data.item.idfolio,
      "limite": data.item.limite,
      "estado": data.item.estado,
      "fecancel": data.item.fecancel
    }

    this.$.updateAccounts.headers.authorization = "Bearer " + this.token;
    this.$.updateAccounts.body = JSON.stringify(modificaData);
    this.$.updateAccounts.generateRequest();
  }

  procesaEvento(e) {
    console.log("Capturado evento del emisor");

    this.$.resumen.hidden = false;
    this.$.cabecera.hidden = false;
    this.$.repeat.hidden = false;
    this.$.mvtos.hidden = true;
  }

  clickOperar(e) {
    console.log("click operar");

    if (!this.cambiolimite){

      if (e.model.item.estado == "CANCELADA"){

        alert("No es posible operar en una cuenta cancelada");
      }else {

        this.$.altamvtos.idUsuario = this.idUsuario,
        this.$.altamvtos.idfolio = e.model.item.idfolio;
        this.$.altamvtos.iban = e.model.item.iban;
        this.$.altamvtos.saldo = e.model.item.saldo;
        this.$.altamvtos.limite = e.model.item.limite;
        this.$.altamvtos.idcuenta = e.model.item.idcuenta;
        this.$.altamvtos.saldoedit = e.model.item.saldo.toLocaleString('es-ES', { style: 'currency', currency: 'EUR' });
        this.$.altamvtos.token = this.token;

        this.$.altamvtos.hidden = false;
        this.$.resumen.hidden = true;
        this.$.cabecera.hidden = true;
        this.$.repeat.hidden = true;
      }
    }else {

      this.$.blockerCambios.hidden = true;

      var elemento = this.shadowRoot.querySelectorAll("#operar")[this.indiceAbierto];
      elemento.textContent = "Operar";

      var elemento = this.shadowRoot.querySelectorAll("#limite")[this.indiceAbierto];
      elemento.value = this.limite;

      this.cambiolimite = false;
    }
  }

  eventoAlta(evento) {

    console.log("recibe evento alta " + evento.detail.evento);

    this.$.altamvtos.hidden = true;

    if (evento.detail.evento == "altaMvto"){

      this.lanzador = new Date();
    }else {

      this.$.resumen.hidden = false;
      this.$.cabecera.hidden = false;
      this.$.repeat.hidden = false;
    }
  }

  finUpdate(data) {

    if (data.detail.response.msg == "Modificación correcta"){

      this.$.blockerCambios.hidden = true;

      var elemento = this.shadowRoot.querySelectorAll("#spinneractualiza")[this.indice];
      elemento.hidden = true;

      var elemento = this.shadowRoot.querySelectorAll("#spinnercancela")[this.indice];
      elemento.hidden = true;

      elemento = this.shadowRoot.querySelectorAll("#operar")[this.indice];
      elemento.textContent = "Operar";

      this.cambiolimite = false;

      this.lanzador = new Date();
    }else {
      alert("Error al actualizar: " + data.detail.response.msg);
    }
  }

  trataErrorUpdate(error) {

    console.log("Hubo un error");
    console.log(error);
    console.log(error.detail.error.message);

    var elemento = this.shadowRoot.querySelectorAll("#spinneractualiza")[this.indice];
    elemento.hidden = true;

    var elemento = this.shadowRoot.querySelectorAll("#spinnercancela")[this.indice];
    elemento.hidden = true;

    alert("Error en actualización: " + error.detail.error.message);
  }
}

window.customElements.define('visor-cuentas', VisorCuentas);
