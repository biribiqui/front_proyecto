import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class AltaUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>

      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
      <link href="estilos.css" rel="stylesheet" type="text/css" />

      <div id="blocker">
      </div>

      <div class="row">
          <div class="shadow-lg col-4 offset-4 container container-comun">
  			<h2>Bienvenido a Bank of Moderdonia</h2>
  			<div class="container-fluid">
  				<div class="row">
  					<div class="offset-4">
  						<img src="src/pagina-principal/imagenes/logo-2.png">
  					</div>
  				</div>
              </div>
        	    </br>
              <div class="card card-alta">
  				</br>
  				<span>Por favor rellene sus datos:</span>
  				</br>
  					<form name="datosalta">
  						<div class="form-group">
  							<input id="nombre" type="text" class="form-control"  placeholder="* Nombre" value="{{nombre::input}}" on-change="validaCompleto" autofocus>
  						</div>
  						<div class="form-group">
  							<input id="apellido" type="text" class="form-control" placeholder="* Apellidos" value="{{apellido::input}}" on-change="validaCompleto">
  						</div>
  						<div class="form-group">
  							<input id="domicilio" type="text" class="form-control" placeholder="Domicilio" value="{{domicilio::input}}">
  						</div>
  						<div class="form-inline">
  							<input id="provincia" type="text" class="form-control mb-3 mr-sm-3" placeholder="Provincia" value="{{provincia::input}}">
    				    <input id="codpostal" type="text" class="form-control mb-3 mr-sm-3" placeholder="* Código Postal" value="{{codpostal::input}}" on-change="validaCompleto">
  						</div>
  						<div class="form-inline">
  							<input id="nif" type="text" class="form-control mb-3 mr-sm-3" placeholder="* NIF" value="{{nif::input}}" on-change="validaCompleto">
  							<input id="telefono" type="text" class="form-control mb-3 mr-sm-3" placeholder="* Número de Teléfono" value="{{telefono::input}}" on-change="validaCompleto">
  						</div>
              <div class="form-inline">
  							<input id="email" type="email" class="form-control mb-3 mr-sm-3" placeholder="* Email" value="{{email::input}}" on-change="validaCompleto">
  						</div>
              <div class="form-inline">
  							<input id="password" type="password" class="form-control mb-3 mr-sm-3" placeholder="* Password" value="{{password::input}}" on-change="validaCompleto">
  							<input id="password2" type="password" class="form-control mb-3 mr-sm-3" placeholder="* Confirme Password" value="{{password2::input}}" on-change="validaCompleto">
  						</div>
  					</form>
  			</div>
    		</br>
        <div id="cajaerror" class="alerterror">
            <span id="erroralta">ERROR:</span>
            </br>
        </div>
  			</br>
        <div class="text-center">
        		<button type="button" class="btn btn-secondary btn-alb" id="btnAlta" on-click="altaUsuario" disabled>Alta Usuario
              <span id="spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true" hidden></span>
            </button>
        		<button type="button" class="btn btn-secondary btn-alb" id="btnAltaCancel" on-click="cancelAlta">Cancelar</button>
        </div>
      </div>

      <iron-ajax
        id="doAlta"
        url="http://localhost:3000/apitechu/users/alta"
        handle-as="json"
        content-type="application/json"
        method="POST"
        on-response="manageAJAXresponse"
        on-error="showError"
      >
      </iron-ajax>
    `;
  }

  static get properties() {
    return {
      nombre: {
        type: String
      }, apellido: {
        type: String
      }, domicilio: {
        type: String
      }, provincia: {
        type: String
      }, codpostal: {
        type: String
      }, nif: {
        type: String
      }, telefono: {
        type: String
      }, email: {
        type: String
      }, password: {
        type: String
      }, password2: {
        type: String
      }, idUsuario: {
        type: Number
      }, token: {
        type: String
      }
    };
  }

  altaUsuario() {
    console.log("Alta pulsado");

    if (this.password != this.password2){

      this.$.erroralta.textContent = "ERROR: clave confirmada incorrecta"
      this.$.cajaerror.style.color = "black";
    }
    else {
      var altaData = {
        "nombre": this.nombre,
        "apellido": this.apellido,
        "domicilio": this.domicilio,
        "provincia": this.provincia,
        "codpostal": this.codpostal,
        "nif": this.nif,
        "telefono": this.telefono,
        "email": this.email,
        "password": this.password,
        "fecalta": new Date()
      }

      this.$.doAlta.body = JSON.stringify(altaData);
      this.$.doAlta.generateRequest();
      this.$.spinner.hidden = false;
    }
  }

  cancelAlta() {
    console.log("Cancelar pulsado");

    this.generaEvento("cancelAlta");

    this.inicializa();
  }

  manageAJAXresponse(data) {

    if (data.detail.response.msg == "Usuario Creado") {

        this.idUsuario = data.detail.response.id;
        this.token = data.detail.response.token;

        this.generaEvento("altaUsuario");

        this.inicializa();
    }
    else {

      this.$.erroralta.textContent = data.detail.response.msg;
      this.$.cajaerror.style.color = "black";
    }

    this.$.spinner.hidden = true;
  }

  showError(error) {

    if (error.detail.request.status == 404){

      this.$.erroralta.textContent = "Usuario y/o password incorrectos";
    }
    else {
      this.$.erroralta.textContent = error.detail.error.message;
    }

    this.$.spinner. hidden = true;
    this.$.cajaerror.style.color = "black";
  }

  generaEvento(evento) {

    this.dispatchEvent(
      new CustomEvent(
        "eventalta",
        {
          detail: {
            "evento": evento,
            "idUsuario" : this.idUsuario,
            "email": this.email,
            "datos": {
              "nombre": this.nombre,
              "apellido": this.apellido,
              "domicilio": this.domicilio,
              "provincia": this.provincia,
              "codpostal": this.codpostal,
              "nif": this.nif,
              "telefono": this.telefono
            },
            "token": this.token
          }
        }
      )
    )
  }

  validaCompleto() {

    if (!this.campoVacio(this.$.nombre.value) && !this.campoVacio(this.$.apellido.value) && !this.campoVacio(this.$.codpostal.value) &&
        !this.campoVacio(this.$.nif.value) && !this.campoVacio(this.$.telefono.value) && !this.campoVacio(this.$.email.value) &&
        !this.campoVacio(this.$.password.value) && this.$.password.value.length > 7) {

      console.log("Datos informados");

      this.$.btnAlta.disabled = false;
      this.$.btnAlta.focus();
    }
  }

  campoVacio(datoEntrada) {

      var retorno = true;

      if (datoEntrada.length > 0 && datoEntrada != "")
      {
        for (var i = 0; i < datoEntrada.length; i++)
        {
          if (datoEntrada[i] != " ")
          {
            retorno = false;
          }
        }
      }

      return retorno;
  }

  inicializa() {

    this.$.cajaerror.style.color = "#E72085";
    this.$.btnAlta.disabled = true;
    this.$.nombre.focus();
    this.nombre = "";
    this.apellido = "";
    this.domicilio = "";
    this.provincia = "";
    this.codpostal = "";
    this.nif = "";
    this.telefono = "";
    this.email = "";
    this.password = "";
    this.password2 = "";
    this.idUsuario = null;
  }
}

window.customElements.define('alta-usuario', AltaUsuario);
