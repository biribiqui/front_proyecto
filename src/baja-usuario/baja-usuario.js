import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class BajaUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>

      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
      <link href="estilos.css" rel="stylesheet" type="text/css" />

      <div id="blocker">
      </div>

      <div class="shadow-lg card card-logout" id="baja">
    		<h4 class="card-title">BAJA DE USUARIO</h4>
    		</br>
        <div>Se va a dar del baja el usuario:
        </div>
  		  </br>
        <span style="font-weight:bold">{{email}}</span>
  		  </br>
        <div>¿Desea continuar?
        </div>
        </br>
        <div id="cajaerror" class="alertaerror">
            <span id="errorbaja">ERROR:</span>
            </br>
        </div>
  			</br>
  	 		<div class="text-center">
    			<button type="button" class="btn btn-secondary btn-formulario" id="btnAceptar" on-click="bajaUsuario">Aceptar
            <span id="spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true" hidden></span>
          </button>
    			<button type="button" class="btn btn-secondary btn-formulario" id="btnCancelar" on-click="cancelaBaja">Cancelar</button>
    		</div>
    	</div>

      <iron-ajax
        id="getAccounts"
        url="http://localhost:3000/apitechu/secure/accounts/{{idUsuario}}"
        handle-as="json"
        on-response="trataRespuestaConsulta"
        on-error="trataErrorConsulta"
      >
      </iron-ajax>

      <iron-ajax
        id="doBaja"
        url="http://localhost:3000/apitechu/secure/users/baja/{{idUsuario}}"
        handle-as="json"
        content-type="application/json"
        method="POST"
        on-response="trataRespuestaBaja"
        on-error="trataErrorBaja"
      >
      </iron-ajax>
    `;
  }

  static get properties() {
    return {
      email: {
        type: String
      }, idUsuario: {
        type: Number
      }, token: {
        type: String
      }
    };
  }

  bajaUsuario() {
    console.log("baja pulsado " + this.idUsuario + " " + this.email);

    this.$.getAccounts.headers.authorization = "Bearer " + this.token;
    this.$.getAccounts.generateRequest();

    this.$.spinner.hidden = false;
  }

  cancelaBaja() {
    console.log("Cancelar pulsado");

    this.$.cajaerror.style.color = "#9F2B57";

    this.generaEvento("cancelBaja");
  }

  trataRespuestaConsulta(data) {

    var correcto = true;

    if (data.detail.response.length > 0){

      for (var i = 0; i < data.detail.response.length; i++){

        if (data.detail.response[i].estado == "ACTIVA" || data.detail.response[i].saldo !=0){

          correcto = false;

          break;
        }
      }
    }

    if (correcto){

      var bajaData = {
        "febaja": new Date()
      }

      this.$.doBaja.headers.authorization = "Bearer " + this.token;
      this.$.doBaja.body = JSON.stringify(bajaData);
      this.$.doBaja.generateRequest();
    }else {

      this.$.errorbaja.textContent = "ERROR: cuentas activas asociadas";
      this.$.cajaerror.style.color = "black";
      this.$.spinner.hidden = true;
    }
  }

  trataRespuestaBaja(data) {

    if (data.detail.response.msg == "Baja correcta") {

        this.generaEvento("bajaUsuario");

        this.inicializa();
    }
    else {

      this.$.errorbaja.textContent = data.detail.response.msg;
      this.$.cajaerror.style.color = "black";
    }

    this.$.spinner.hidden = true;
  }

  trataErrorConsulta(error) {

    if (error.detail.request.status == 404){

      var bajaData = {
        "febaja": new Date()
      }

      this.$.doBaja.headers.authorization = "Bearer " + this.token;
      this.$.doBaja.body = JSON.stringify(bajaData);
      this.$.doBaja.generateRequest();
    }
    else {
      this.$.errorbaja.textContent = error.detail.error.message;
    }

    this.$.spinner. hidden = true;
    this.$.cajaerror.style.color = "black";
  }

  trataErrorBaja(error) {

    if (error.detail.request.status == 404){

      this.$.errorbaja.textContent = "Usuario incorrecto";
    }
    else {
      this.$.errorbaja.textContent = error.detail.error.message;
    }

    this.$.spinner. hidden = true;
    this.$.cajaerror.style.color = "black";
  }

  generaEvento(evento) {

    this.dispatchEvent(
      new CustomEvent(
        "eventbaja",
        {
          detail: {
            "evento": evento,
            "idUsuario" : this.idUsuario
          }
        }
      )
    )
  }

  inicializa() {

    this.$.cajaerror.style.color = "#9F2B57";
  }
}

window.customElements.define('baja-usuario', BajaUsuario);
