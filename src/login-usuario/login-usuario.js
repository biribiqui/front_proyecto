import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class LoginUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>

      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
      <link href="estilos.css" rel="stylesheet" type="text/css" />

      <div id="blocker">
      </div>

      <div class="shadow-lg card card-login" id="login">
    		<h4 class="card-title">LOG IN</h4>
    		</br>
    		<form name="datoslogin">
    			<div class="form-group">
    				<label>Correo Electrónico</label>
    					<input id="email" type="email" class="form-control"  value="{{email::input}}" on-change="validaCompleto" autofocus>
    			</div>
    			<div class="form-group">
    				<label>Contraseña</label>
    				<input id="password" type="password" class="form-control" value="{{password::input}}" on-change="validaCompleto">
    			</div>
    		</form>
    		</br>
        <div id="cajaerror" class="alertaerror">
          <span id="errorlogin">ERROR:</span>
          </br>
        </div>
    		</br>
    		</br>

    		<div class="text-center">
    			<button type="button" class="btn btn-secondary btn-formulario" id="btnLogin" on-click="loginUsuario" disabled>Log In
            <span id="spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true" hidden></span>
          </button>
    			<button type="button" class="btn btn-secondary btn-formulario" id="btnLogCancel" on-click="cancelaLogIn">Cancelar</button>
    		</div>
    	</div>

      <iron-ajax
        id="doLogin"
        url="http://localhost:3000/apitechu/users/login"
        handle-as="json"
        content-type="application/json"
        method="POST"
        on-response="manageAJAXresponse"
        on-error="showError"
      >
      </iron-ajax>
    `;
  }

  static get properties() {
    return {
      email: {
        type: String
      }, password: {
        type: String
      }, idUsuario: {
        type: Number
      }
    };
  }

  loginUsuario() {
    console.log("Log In pulsado");

    var loginData = {
      "email": this.email,
      "password": this.password
    }

    this.$.doLogin.body = JSON.stringify(loginData);
    this.$.doLogin.generateRequest();

    this.$.spinner.hidden = false;
  }

  cancelaLogIn() {
    console.log("Cancelar pulsado");

    this.inicializa();

    this.generaEventoCancel("cancelaLogIn");
  }

  manageAJAXresponse(data) {

    if (data.detail.response.msg == "Login correcto") {

        this.idUsuario = data.detail.response.idUsuario;

        this.generaEvento("logIn", data.detail.response);

        this.inicializa();
    }
    else {

      this.$.errorlogin.textContent = "ERROR: " + data.detail.response.msg;
      this.$.cajaerror.style.color = "black";
    }

    this.$.spinner.hidden = true;
  }

  showError(error) {

    if (error.detail.request.status == 404){

      this.$.errorlogin.textContent = "ERROR: datos incorrectos";
    }
    else {
      this.$.errorlogin.textContent = "ERROR: " + error.detail.error.message;
    }

    this.$.cajaerror.style.color = "black";
    this.$.spinner.hidden = true;
  }

  generaEvento(evento, datosLogin) {

    this.dispatchEvent(
      new CustomEvent(
        "eventlogin",
        {
          detail: {
            "evento": evento,
            "idUsuario" : this.idUsuario,
            "email": this.email,
            "datos": {
              "nombre": datosLogin.first_name,
              "apellido": datosLogin.apellido,
              "domicilio": datosLogin.domicilio,
              "provincia": datosLogin.provincia,
              "codpostal": datosLogin.codpostal,
              "nif": datosLogin.nif,
              "telefono": datosLogin.telefono
            },
            "token":datosLogin.token
          }
        }
      )
    )
  }

  generaEventoCancel(evento) {

    this.dispatchEvent(
      new CustomEvent(
        "eventlogin",
        {
          detail: {
            "evento": evento
          }
        }
      )
    )
  }

  validaCompleto() {

    if (this.$.email.value.length > 0 && this.$.password.value.length > 0){

      this.$.btnLogin.disabled = false;
      this.$.btnLogin.focus();
    }
  }

  inicializa() {

    this.$.cajaerror.style.color = "#9F2B57";
    this.$.btnLogin.disabled = true;
    this.$.email.focus();

    this.email = "";
    this.password = "";
  }
}

window.customElements.define('login-usuario', LoginUsuario);
