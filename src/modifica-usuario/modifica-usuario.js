import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class ModificaUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>

      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
      <link href="estilos.css" rel="stylesheet" type="text/css"/>

      <div id="blocker">
      </div>

      <div class="row">
          <div class="shadow-lg col-4 offset-4 container container-comun">
  			<h2>Bank of Moderdonia</h2>
  			<div class="container-fluid">
  				<div class="row">
  					<div class="offset-4">
  						<img src="src/pagina-principal/imagenes/logo-2.png">
  					</div>
  				</div>
          </div>
          </br>
          <div class="card card-alta">
  				  </br>
  					<form name="datosconsulta">
  						<div class="form-group">
						    <label>* Nombre</label>
  							<input id="nombre" type="text" class="form-control" value="{{datos.nombre::input}}">
  						</div>
  						<div class="form-group">
						    <label>* Apellidos</label>
  							<input id="apellido" type="text" class="form-control" value="{{datos.apellido::input}}">
  						</div>
  						<div class="form-group">
						    <label>Domicilio</label>
  							<input id="domicilio" type="text" class="form-control" value="{{datos.domicilio::input}}">
  						</div>
						  <label>Provincia / * Código Postal</label>
  						<div class="form-inline">
  							<input id="provincia" type="text" class="form-control mb-3 mr-sm-3" value="{{datos.provincia::input}}">
    				    <input id="codpostal" type="text" class="form-control mb-3 mr-sm-3" value="{{datos.codpostal::input}}">
  						</div>
						  <label>* NIF / * Teléfono</label>
  						<div class="form-inline">
  							<input id="nif" type="text" class="form-control mb-3 mr-sm-3" value="{{datos.nif::input}}">
	 							<input id="telefono" type="text" class="form-control mb-3 mr-sm-3" value="{{datos.telefono::input}}">
  						</div>
  					</form>
  			</div>
    		</br>
        <div id="cajaerror" class="alerterror">
            <span id="errormodifica">ERROR:</span>
            </br>
        </div>
  			</br>
        <div class="text-center">
            <button type="button" class="btn btn-secondary btn-alb" id="btnModifica" on-click="modificaUsuario">Actualizar Usuario
              <span id="spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true" hidden></span>
            </button>
        		<button type="button" class="btn btn-secondary btn-alb" id="btnCancelModifica" on-click="cancelaModifica">Cancelar</button>
        </div>
      </div>

      <iron-ajax
        id="doModifica"
        url="http://localhost:3000/apitechu/secure/users/modifica/{{idUsuario}}"
        handle-as="json"
        content-type="application/json"
        method="POST"
        on-response="manageAJAXresponse"
        on-error="showError"
      >
      </iron-ajax>

    `;
  }

  static get properties() {
    return {
      datos: {
        type: Object
      }, idUsuario: {
        type: Number
      }, email: {
        type: String
      }, token: {
        type: String
      }
    };
  }

  cancelaModifica() {

    console.log("Cancelar pulsado");

    this.generaEvento("cancelaModifica");
  }

  modificaUsuario() {

    console.log("Modificar pulsado");

    if (this.validaDatos()){

      var modificaData = {
        "nombre": this.datos.nombre,
        "apellido": this.datos.apellido,
        "domicilio": this.datos.domicilio,
        "provincia": this.datos.provincia,
        "codpostal": this.datos.codpostal,
        "nif": this.datos.nif,
        "telefono": this.datos.telefono
      }

      this.$.doModifica.headers.authorization = "Bearer " + this.token;
      this.$.doModifica.body = JSON.stringify(modificaData);
      this.$.doModifica.generateRequest();

      this.$.spinner.hidden = false;
    }
  }

  validaDatos() {

    var correcto = true;

    if (this.campoVacio(this.datos.nombre)){

      this.$.nombre.focus();

      correcto = false;
    }

    if (this.campoVacio(this.datos.apellido)){

      this.$.apellido.focus();

      correcto = false;
    }

    if (this.campoVacio(this.datos.codpostal)){

      this.$.codpostal.focus();

      correcto = false;
    }

    if (this.campoVacio(this.datos.nif)){

      this.$.nif.focus();

      correcto = false;
    }

    if (this.campoVacio(this.datos.telefono)){

      this.$.telefono.focus();

      correcto = false;
    }

    if (!correcto){

      this.$.errormodifica.textContent = "ERROR: Campo Obligatorio sin Informar";
      this.$.cajaerror.style.color = "black";
    }

    return correcto;
  }

  campoVacio(datoEntrada) {

      var retorno = true;

  		if (datoEntrada.length > 0 && datoEntrada != "")
  		{
  			for (var i = 0; i < datoEntrada.length; i++)
  			{
  				if (datoEntrada[i] != " ")
  				{
  					retorno = false;
  				}
  			}
  		}

  		return retorno;
	}

  manageAJAXresponse(data) {

    if (data.detail.response.msg == "Modificación correcta") {

        this.generaEvento("modificaUsuario");
    }
    else {

      this.$.errormodifica.textContent = "ERROR" + data.detail.response.msg;
      this.$.cajaerror.style.color = "black";
    }

    this.$.spinner.hidden = true;
  }

  showError(error) {

    if (error.detail.request.status == 404){

      this.$.errorModifica.textContent = "ERROR: Usuario incorrecto";
    }
    else {
      this.$.errorModifica.textContent = error.detail.error.message;
    }

    this.$.spinner. hidden = true;
    this.$.cajaerror.style.color = "black";
  }

  generaEvento(evento) {

    this.dispatchEvent(
      new CustomEvent(
        "eventmodifica",
        {
          detail: {
            "evento": evento,
            "idUsuario": this.idUsuario,
            "email": this.email,
            "datos": this.datos
          }
        }
      )
    )
  }
}

window.customElements.define('modifica-usuario', ModificaUsuario);
