import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@vaadin/vaadin-grid/vaadin-grid.js';

/**
 * @customElement
 * @polymer
 */
class ConsultaPrestamo extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          background-color: #F0507F;
        }
      </style>

      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
      <link href="estilos.css" rel="stylesheet" type="text/css" />

      <div id="formulario" class="container container-alta shadow-lg">
        </br>
        </br>
        <h2 class="text-center" style="font-weight: bold">Cálculo de Préstamo</h2>
  			</br>
        </br>
        <p class="offset-2">Por favor rellene los datos:</p>
        </br>
  			<form name="datoptmo">
          <div class="form-row">
            <div class="form-group offset-2">
              <label>Importe Financiado</label>
      				<input id="importe" type="text" class="form-control" maxlength="10" on-change="validaImporteFinanciado">
    				</div>
            <div class="form-group offset-1">
              <label>Numero Cuotas:</label>
      				<input id="numcuotas" type="text" class="form-control" maxlength="3" on-change="validaNumcuotas">
    				</div>
          </div>
          </br>
          <div class="form-row">
            <div class="form-group offset-2">
              <label>Tipo Amortización:</label>
              <select id="sisamor" class="form-control" on-change="cambioSistema">
                <option value="cFRA">Cuota Constante</option>
                <option value="cFIN">Cuota Constante y Final</option>
              </select>
            </div>
            <div class="form-group offset-1">
              <label>Importe Cuota Final</label>
      				<input id="cuofinal" type="text" class="form-control" maxlength="10" on-change="validaImporteFinal" disabled>
    				</div>
          </div>
          </br>
          <div class="form-row">
            <div class="form-group offset-2">
              <label>Periodicidad de Amortización:</label>
              <select id="peramor" class="form-control">
                <option value="men">Mensual</option>
                <option value="tri">Trimestral</option>
                <option value="sem">Semestral</option>
                <option value="anu">Anual</option>
              </select>
            </div>
            <div class="form-group offset-1">
              <label>Porcentaje Interés:</label>
      				<input id="porcinteres" type="text" class="form-control" maxlength="6" on-change="validaInteres">
    				</div>
          </div>
  			</form>
    		</br>
        <div id="cajaerror" class="alerterror">
            <span id="errorptmo" style="color: #FFD2D2">ERROR:</span>
            </br>
        </div>
  			</br>
        </br>
        </br>
        <div class="text-center">
        		<button type="button" class="btn btn-secondary btn-alb" id="btnptmo" on-click="calculoPrestamo" disabled>Calcular
              <span id="spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true" hidden></span>
            </button>
        		<button type="button" class="btn btn-secondary btn-alb" id="btncancelar" on-click="cancelCalculo">Cancelar</button>
        </div>
        </br>
      </div>

      <div id="cuadro" class="container text-right shadow-lg">
        </br>
        </br>
        <h3 class="text-center" style="font-weight: bold">SU PRESTAMO</h3>
        </br>
        <div class="row" style="font-weight: bold">
          <p id="impfinan" class="offset-1">Importe Financiado: [[impFinan]]</p>
          <p id="plazo" class="offset-1">Num. Cuotas: [[plazo]]</p>
          <p id="interes" class="offset-1">% Interés: [[interes]]</p>
          <p id="tae" class="offset-1">TAE: [[tae]]</p>
        </div>
        </br>
        <vaadin-grid id="vaadingrid" theme="no-border" style="height: 410px">
          <vaadin-grid-column path="numcuota" header="Número de Cuota"></vaadin-grid-column>
          <vaadin-grid-column path="imppdte" header="Capital Pendiente"></vaadin-grid-column>
          <vaadin-grid-column path="impcapital" header="Importe Capital"></vaadin-grid-column>
          <vaadin-grid-column path="impinteres" header="Importe Intereses"></vaadin-grid-column>
          <vaadin-grid-column path="impcuota" header="Importe Cuota"></vaadin-grid-column>
        </vaadin-grid>
        </br>
        </br>
        <div class="text-center">
            <button type="button" class="btn btn-secondary btn-alb" id="btnatras" on-click="_inicializaPagina">Nuevo Cálculo</button>
        </div>
      </div>

      <iron-ajax
        id="doCalculo"
        url="http://localhost:3000/apicuadro/loans/cuadro/6102ssaPesooM"
        handle-as="json"
        content-type="application/json"
        method="POST"
        on-response="tratarRespuesta"
        on-error="tratarError"
      >
      </iron-ajax>

    `;
  }

  static get properties() {
    return {
      lanzador: {
        type: Date,
        observer: "_inicializaPagina"
      }, impFinan: {
        type: String
      }, plazo: {
        type: Number
      }, interes: {
        type: Number
      }, tae: {
        type: Number
      }
    };
  }

  _inicializaPagina () {

      this.inicializa();
      this.$.formulario.hidden = false;
      this.$.cuadro.hidden = true;
  }

  calculoPrestamo() {
    console.log("Calculo prestamo");

    this.impFinan = Number(this.$.importe.value).toLocaleString('es-ES', { style: 'currency', currency: 'EUR' });
    this.plazo = Number(this.$.numcuotas.value);
    this.interes = Number(this.$.porcinteres.value);
    this.tae = 0;

    var datosCalculo = {
    	"fechaInicio":["2019","03","31"],
    	"importeInicial":Number(this.$.importe.value),
    	"diaPago":31,
      "numCuotas":Number(this.$.numcuotas.value),
      "perAmo":this.$.peramor.value,
      "sisAmr":this.$.sisamor.value,
      "indCapAjuste":false,
    	"claInt":"F",
    	"aplicaLCI":false,
    	"intInicial":Number(this.$.porcinteres.value),
    	"mesesIntini":Number(this.$.numcuotas.value),
    	"intMixto":0,
      "indCare":false,
      "mesesCare":0,
      "perCare":"",
      "tipAnno":"",
      "porcInc":0,
      "cuoFinal":Number(this.$.cuofinal.value),
      "impCuota":0,
      "impCuota":0,
      "indAjuste":false,
      "capAjuste":0,
      "fechaAjuste":"",
      "indAmplia":false,
      "impAmplia":0,
      "fecAmplia":""
    }

    this.$.doCalculo.body = JSON.stringify(datosCalculo);
    this.$.doCalculo.generateRequest();

    this.$.spinner.hidden = false;
  }

  cancelCalculo() {
    console.log("Cancelar pulsado");

    this.generaEvento("cancelCalculoPtmo");

    this.inicializa();
  }

  tratarRespuesta(data) {

    if (data.detail.response.msg == "Cuadro Calculado") {

      if (data.detail.response.cuadro.length > 0){

        this.tae = Math.round(data.detail.response.porcTae * 10000) / 10000;

        var cuadroRespuesta = [];
        var acumCapital = 0;
        var acumInteres = 0;
        var acumTotal = 0;

        for (var i = 0; i < data.detail.response.cuadro.length; i++){

          var cuotaRespuesta = {};

          acumCapital += data.detail.response.cuadro[i].imp_capital;
          acumInteres += data.detail.response.cuadro[i].imp_interes
          cuotaRespuesta.numcuota = i + 1;
          cuotaRespuesta.imppdte = data.detail.response.cuadro[i].imp_pdte.toLocaleString('es-ES', { style: 'currency', currency: 'EUR' });
          cuotaRespuesta.impcapital = data.detail.response.cuadro[i].imp_capital.toLocaleString('es-ES', { style: 'currency', currency: 'EUR' });
          cuotaRespuesta.impinteres = data.detail.response.cuadro[i].imp_interes.toLocaleString('es-ES', { style: 'currency', currency: 'EUR' });

          var impcuota = Math.round((data.detail.response.cuadro[i].imp_capital + data.detail.response.cuadro[i].imp_interes) * 100) / 100;

          cuotaRespuesta.impcuota = impcuota.toLocaleString('es-ES', { style: 'currency', currency: 'EUR' });
          acumTotal += impcuota;

          cuadroRespuesta.push(cuotaRespuesta);
        }

        cuotaRespuesta = {};
        cuotaRespuesta.numcuota = "TOTAL";
        cuotaRespuesta.impcapital = (Math.round(acumCapital * 100) / 100).toLocaleString('es-ES', { style: 'currency', currency: 'EUR' });
        cuotaRespuesta.impinteres = (Math.round(acumInteres * 100) / 100).toLocaleString('es-ES', { style: 'currency', currency: 'EUR' });
        cuotaRespuesta.impcuota = (Math.round(acumTotal * 100) / 100).toLocaleString('es-ES', { style: 'currency', currency: 'EUR' });

        cuadroRespuesta.push(cuotaRespuesta);

        this.$.vaadingrid.items = cuadroRespuesta;

        this.$.formulario.hidden = true;
        this.$.cuadro.hidden = false;
      }else {

        this.$.errorptmo.textContent = "No ha sido posible calcular el cuadro"
        this.$.cajaerror.style.color = "black";
      }
    }else {

      this.$.errorptmo.textContent = data.detail.response.msg;
      this.$.cajaerror.style.color = "black";
    }

    this.$.spinner.hidden = true;
  }

  tratarError(error) {

    if (error.detail.request.status == 404){

      this.$.errorptmo.textContent = "No fue posible establecer la conexión";
    }
    else {
      this.$.errorptmo.textContent = error.detail.error.message;
    }

    this.$.spinner.hidden = true;
    this.$.cajaerror.style.color = "black";
  }

  generaEvento(evento) {

    this.dispatchEvent(
      new CustomEvent(
        "eventptmo",
        {
          detail: {
            "evento": evento
          }
        }
      )
    )
  }

  validaImporteFinanciado() {

    if (isNaN(Number(this.$.importe.value))){

      alert("ERROR: el importe financiado debe ser numérico");

      this.$.importe.focus();
    }else {

      if(Number(this.$.importe.value) < 0){

        alert("ERROR: el importe financiado debe ser positivo");

        this.$.importe.focus();
      }else {

        this.validaDatos();
      }
    }
  }

  validaNumcuotas() {

    if (isNaN(Number(this.$.numcuotas.value))){

      alert("ERROR: el número de cuotas debe ser numérico");

      this.$.numcuotas.focus();
    }else {

      if(Number(this.$.numcuotas.value) < 0){

        alert("ERROR: el número de cuotas debe ser positivo");

        this.$.numcuotas.focus();
      }else {

        if (Number(this.$.numcuotas.value) > 500){

          alert("ERROR: el número de cuotas no puede superar 500");

          this.$.numcuotas.focus();
        }else {

          this.validaDatos();
        }
      }
    }
  }

  cambioSistema() {

    if (this.$.sisamor.value == "cFIN"){

      this.$.cuofinal.disabled = false;
    }else {
      this.$.cuofinal.disabled = true;
      this.$.cuofinal.value = "";
    }
  }

  validaImporteFinal() {

    if (isNaN(Number(this.$.cuofinal.value))){

      alert("ERROR: el importe de cuota final debe ser numérico");

      this.$.cuofinal.focus();
    }else {

      if(Number(this.$.cuofinal.value) < 0){

        alert("ERROR: el importe de cuota final debe ser positivo");

        this.$.cuofinal.focus();
      }else {

        if (Number(this.$.cuofinal.value) > Number(this.$.importe.value)){

          alert("ERROR: el importe de cuota final no puede superar el financiado");

          this.$.cuofinal.focus();
        }else {

          this.validaDatos();
        }
      }
    }
  }

  validaInteres() {

    if (isNaN(Number(this.$.porcinteres.value))){

      alert("ERROR: el % interes debe ser numérico");

      this.$.porcinteres.focus();
    }else {

      if(Number(this.$.porcinteres.value) < 0){

        alert("ERROR: el % interes debe ser positivo");

        this.$.porcinteres.focus();
      }else {

        if (Number(this.$.porcinteres.value) > 100){

          alert("ERROR: el % interese no puede superar el 100%");

          this.$.porcinteres.focus();
        }else {

          this.validaDatos();
        }
      }
    }
  }

  validaDatos() {

    if (!this.campoVacio(this.$.importe.value) && !this.campoVacio(this.$.numcuotas.value) &&
        !this.campoVacio(this.$.porcinteres.value) && (this.$.sisamor.value != "cFIN" ||
        (this.$.sisamor.value == "cFIN" && !this.campoVacio(this.$.cuofinal.value)))){

      this.$.btnptmo.disabled = false;
    }
  }

  campoVacio(datoEntrada) {

      var retorno = true;

      if (datoEntrada.length > 0 && datoEntrada != ""){

        for (var i = 0; i < datoEntrada.length; i++)
        {
          if (datoEntrada[i] != " ")
          {
            retorno = false;
          }
        }
      }

      return retorno;
  }

  inicializa() {

    this.$.cajaerror.style.color = "#FFD2D2";
    this.$.btnptmo.disabled = true;
    this.$.importe.focus();
    this.$.importe.value = "";
    this.$.numcuotas.value = "";
    this.$.sisamor.value = "cFRA";
    this.$.cuofinal.value = "";
    this.$.peramor.value = "men";
    this.$.porcinteres.value = "";
  }
}

window.customElements.define('consulta-prestamo', ConsultaPrestamo);
