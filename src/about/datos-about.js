import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@em-polymer/google-map/google-map.js';
import '@em-polymer/google-map/google-map-marker.js';
//import './shared-styles.js';

/**
 * @customElement
 * @polymer
 */
class DatosAbout extends PolymerElement {
  static get template() {
    return html`
    <style include="shared-styles">
       :host {
         display: block;

         padding: 10px;
         color: white;
       }

       google-map {
         height: 600px;
         width: 100%;
       }
     </style>

      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <link href="estilos.css" rel="stylesheet" type="text/css">

      <div class="row">
        <div id="eltiempo" class="container col-2 text-left">
          <h3>Moderdonia</h3>
          <p> [[condicionNombre]]</p>
          <div class="row">
            <div id="weatherimg" class="container">
              <img id="wicon" src="" width="120%" height="120%">
            </div>
            <div>
              <p style="font-size: 60px; margin-left: 10px">[[temperatura]]</p>
            </div>
            <div>
              <p style="font-size: large"> &#176C | &#176F</p>
            </div>
          </div>
        </div>

        <div id="imagen1" class="container offset-2">
          <img src="src/about/imagenes/vida-moderna-25518.jpg">
        </div>

        <div id="imagen2" class="container offset-2">
          <img src="src/about/imagenes/moderdonia_web.png">
        </div>

        <div id="imagen3" class="container offset-2">
          <img src="src/about/imagenes/450_1000.jpg">
        </div>
      </div>

      <div id="quien" class="container" style="font-size: large">
        <audio src="src/about/audio/Ipno de Moderdonia.mp3" controls>
        </audio>
        </br>
        </br>
        <h2>¿Quienes somos?</h2>
        </br>
        <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas leo diam, accumsan ut pulvinar id, faucibus et mauris. Mauris ligula mauris, suscipit in justo a, malesuada sodales lacus. Quisque sed arcu ornare, elementum nisi vitae, dictum metus. Proin nibh nunc, aliquet ac nisi ullamcorper, sodales tempus tortor. Donec sed diam vel lectus aliquet imperdiet. Praesent in elementum quam. Quisque condimentum iaculis accumsan. Nunc justo tortor, viverra non magna id, molestie varius mi. Curabitur a varius erat. Sed gravida, velit eget venenatis ultrices, ante nisl euismod quam, quis vehicula purus turpis ut ipsum. Cras hendrerit risus eget massa finibus posuere. Pellentesque lectus ante, dapibus a risus sed, fringilla aliquet ex. Mauris lacinia aliquet ligula, ornare tincidunt lorem tristique a. Aliquam ut justo pretium, placerat ipsum at, maximus nulla. Nunc a aliquet purus. Quisque diam purus, dignissim nec velit at, gravida vestibulum tellus.
        </br>
        </br>
        Fusce eget nisl a turpis tempor porta in vel dui. Donec augue arcu, dapibus vitae ante et, vehicula commodo urna. Fusce porttitor vel sapien tincidunt commodo. Fusce id ipsum et nulla ultricies efficitur sit amet vitae velit. Nullam scelerisque auctor placerat. Vestibulum non volutpat neque, nec vestibulum neque. Duis aliquam libero eu euismod molestie. Nam pretium fringilla ex in elementum. Ut accumsan, urna eu rutrum pharetra, metus urna dapibus ante, eu ullamcorper lorem quam sed eros. Quisque vulputate massa nec est dictum, gravida lacinia diam consectetur. Etiam laoreet sollicitudin justo, in lobortis ante vehicula eu.
        </br>
        </br>
        Curabitur et felis a tellus sollicitudin tempus. Nulla facilisi. Etiam a blandit ligula. Donec mollis, velit vestibulum tristique mollis, mi nulla porta sem, sed iaculis turpis neque viverra arcu. Nulla congue vulputate turpis sed fermentum. Integer convallis neque eu nulla volutpat pretium. Suspendisse sed cursus sem, in fringilla dui. Quisque accumsan pulvinar erat, eget venenatis tellus sagittis vitae. Mauris eu volutpat mauris. Ut vestibulum lacus quis dolor laoreet bibendum. Praesent quis consectetur tortor. Donec ut posuere est. Sed vitae dictum mauris, ut tempus orci. Sed non vehicula libero, eget ornare odio. Aenean porttitor, neque vitae luctus venenatis, diam arcu hendrerit nisl, eu fermentum nibh turpis sed purus. Suspendisse erat lacus, venenatis non fringilla vitae, tempus id nisl.
        </br>
        </br>
        Etiam consectetur lorem eget viverra luctus. Donec urna augue, interdum vel varius vel, eleifend vel metus. Mauris in ultricies odio. In non tincidunt ex. Morbi vehicula elementum nibh quis posuere. Quisque accumsan nulla pellentesque nulla hendrerit, vel blandit neque tristique. Suspendisse sit amet quam ut odio posuere interdum non vitae leo. Mauris fringilla nisl in felis placerat sagittis.
        </br>
        </br>
        Ut orci ante, egestas vitae venenatis vitae, viverra in orci. Maecenas faucibus neque ut faucibus vehicula. Pellentesque accumsan porta odio ut aliquet. Etiam viverra nisl augue. Praesent molestie, eros id finibus ultricies, tortor velit lacinia metus, posuere eleifend lacus lorem accumsan urna. Nunc volutpat dictum est. Phasellus at vestibulum elit. Maecenas nec urna rhoncus, laoreet lacus et, pretium orci. Vivamus pulvinar aliquet dui, ut pellentesque tellus viverra et.
        </br>

        </p>
        </br>
      </div>

      <div id="donde" class="container" style="font-size: large">
        </br>
        </br>
        <h2>¿Donde estamos?</h2>
        </br>
        <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas leo diam, accumsan ut pulvinar id, faucibus et mauris. Mauris ligula mauris, suscipit in justo a, malesuada sodales lacus. Quisque sed arcu ornare, elementum nisi vitae, dictum metus. Proin nibh nunc, aliquet ac nisi ullamcorper, sodales tempus tortor. Donec sed diam vel lectus aliquet imperdiet. Praesent in elementum quam. Quisque condimentum iaculis accumsan. Nunc justo tortor, viverra non magna id, molestie varius mi. Curabitur a varius erat. Sed gravida, velit eget venenatis ultrices, ante nisl euismod quam, quis vehicula purus turpis ut ipsum. Cras hendrerit risus eget massa finibus posuere. Pellentesque lectus ante, dapibus a risus sed, fringilla aliquet ex. Mauris lacinia aliquet ligula, ornare tincidunt lorem tristique a. Aliquam ut justo pretium, placerat ipsum at, maximus nulla. Nunc a aliquet purus. Quisque diam purus, dignissim nec velit at, gravida vestibulum tellus.
        </p>
        </br>
        <h4>Oficina 1133 - Moderdonia Central</h4>
        </br>
        <google-map latitude="40.3571000" longitude="-5.5236500" min-zoom="9" max-zoom="20" language="es" api-key="[[mapsApiKey]]">
          <google-map-marker latitude="40.3571000" longitude="-5.5236500" title="Pura Moderdonia!"></google-map-marker>
        </google-map>
        </br>
        <h4>Oficinas 1134 - Territorio Conquistado</h4>
        </br>
       <google-map latitude="40.4743021" longitude="-5.3311329" min-zoom="9" max-zoom="20" language="es" api-key="[[mapsApiKey]]">
          <google-map-marker latitude="40.4743021" longitude="-5.3311329" title="Territorio conquistado"></google-map-marker>
        </google-map> 
        </br>
      </div>

      <div id="equipo" class="container" style="font-size: large">
        </br>
        </br>
        <h2>¡El Equipo!</h2>
        </br>
        <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas leo diam, accumsan ut pulvinar id, faucibus et mauris. Mauris ligula mauris, suscipit in justo a, malesuada sodales lacus. Quisque sed arcu ornare, elementum nisi vitae, dictum metus. Proin nibh nunc, aliquet ac nisi ullamcorper, sodales tempus tortor. Donec sed diam vel lectus aliquet imperdiet. Praesent in elementum quam. Quisque condimentum iaculis accumsan. Nunc justo tortor, viverra non magna id, molestie varius mi. Curabitur a varius erat. Sed gravida, velit eget venenatis ultrices, ante nisl euismod quam, quis vehicula purus turpis ut ipsum. Cras hendrerit risus eget massa finibus posuere. Pellentesque lectus ante, dapibus a risus sed, fringilla aliquet ex. Mauris lacinia aliquet ligula, ornare tincidunt lorem tristique a. Aliquam ut justo pretium, placerat ipsum at, maximus nulla. Nunc a aliquet purus. Quisque diam purus, dignissim nec velit at, gravida vestibulum tellus.
        </p>
        </br>
        <div class="card-deck">
          <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="src/about/imagenes/imagen-sin-titulo.jpg" alt="Card image cap">
             <div class="card-body">
               <p class="card-text">David Broncano: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas leo diam, accumsan ut pulvinar id, faucibus et mauris.
               </p>
             </div>
          </div>
          <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="src/about/imagenes/69-418x372.png" alt="Card image cap">
             <div class="card-body">
               <p class="card-text">Quequé: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas leo diam, accumsan ut pulvinar id, faucibus et mauris.
               </p>
             </div>
          </div>
          <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="src/about/imagenes/ignatius_0.jpg" alt="Card image cap">
             <div class="card-body">
               <p class="card-text">Ignatius Farray: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas leo diam, accumsan ut pulvinar id, faucibus et mauris.
               </p>
             </div>
          </div>
        </div>
        </br>
        </br>
      </div>

      <iron-ajax
        id="getWeather"
        auto
        url="http://api.openweathermap.org/data/2.5/weather?q=El Barco de Avila&APPID=1ff58c4ac904a511e5266d897e64c168&units=metric"
        handle-as="json"
        on-response="showData"
        on-error="trataError"
      >
      </iron-ajax>

    `;
  }

  static get properties() {
    return {
        weatherApiKey: {
        type: String,
        value : "1ff58c4ac904a511e5266d897e64c168"
      }, mapsApiKey: {
        type: String,
        value: "AIzaSyDGgU9xAfOfByHWlRP1FNmhWm_tK54RYbo"
      }, temperatura: {
        type: Number
      }, condicionNombre: {
        type: String
      }, urlicono: {
        type: String
      }, opcion: {
        type: String,
        observer: "_opcionPulsada"
      }
    };
  }

  _opcionPulsada() {

    switch (this.opcion)
    {
      case "quien":

        this.$.quien.hidden = false;
        this.$.imagen1.hidden = false;
        this.$.donde.hidden = true;
        this.$.imagen2.hidden = true;
        this.$.equipo.hidden = true;
        this.$.imagen3.hidden = true;

        break;

      case "donde":

        this.$.quien.hidden = true;
        this.$.imagen1.hidden = true;
        this.$.donde.hidden = false;
        this.$.imagen2.hidden = false;
        this.$.equipo.hidden = true;
        this.$.imagen3.hidden = true;

        break;

      case "equipo":

      this.$.quien.hidden = true;
      this.$.imagen1.hidden = true;
      this.$.donde.hidden = true;
      this.$.imagen2.hidden = true;
      this.$.equipo.hidden = false;
      this.$.imagen3.hidden = false;

        break;

    }
  }

  showData(data) {
    console.log("showData");

    this.condicionNombre = data.detail.response.weather[0].main;
    this.temperatura = Math.round(data.detail.response.main.temp);
    this.urlicono = "http://openweathermap.org/img/w/" + data.detail.response.weather[0].icon + ".png";

    this.$.wicon.src = this.urlicono;

    this.$.eltiempo.hidden = false;
  }

  trataError(error) {
    console.log("Hubo un error");
    console.log(error);
    console.log(error.detail.error.message);

    this.$.eltiempo.hidden = true;
  }
}

window.customElements.define('datos-about', DatosAbout);
