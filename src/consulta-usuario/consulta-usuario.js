import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * @customElement
 * @polymer
 */
class ConsultaUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>

      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
      <link href="estilos.css" rel="stylesheet" type="text/css" />

      <div id="blocker">
      </div>

      <div class="row">
          <div class="shadow-lg col-4 offset-4 container container-comun">
  			<h2>Bank of Moderdonia</h2>
  			<div class="container-fluid">
  				<div class="row">
  					<div class="offset-4">
  						<img src="src/pagina-principal/imagenes/logo-2.png">
  					</div>
  				</div>
          </div>
          </br>
          <div class="card card-alta">
  				  </br>
  					<form name="datosconsulta">
  						<div class="form-group">
						    <label>Nombre</label>
  							<input id="nombre" type="text" class="form-control" value="{{nombre::input}}" disabled>
  						</div>
  						<div class="form-group">
						    <label>Apellidos</label>
  							<input id="apellido" type="text" class="form-control" value="{{apellido::input}}" disabled>
  						</div>
  						<div class="form-group">
						    <label>Domicilio</label>
  							<input id="domicilio" type="text" class="form-control" value="{{domicilio::input}}" disabled>
  						</div>
						  <label>Provincia / Código Postal</label>
  						<div class="form-inline">
  							<input id="provincia" type="text" class="form-control mb-3 mr-sm-3" value="{{provincia::input}}" disabled>
    				    <input id="codpostal" type="text" class="form-control mb-3 mr-sm-3" value="{{codpostal::input}}" disabled>
  						</div>
						  <label>NIF / Teléfono</label>
  						<div class="form-inline">
  							<input id="nif" type="text" class="form-control mb-3 mr-sm-3" value="{{nif::input}}" disabled>
	 							<input id="telefono" type="text" class="form-control mb-3 mr-sm-3" value="{{telefono::input}}" disabled>
  						</div>
  					</form>
  			</div>
    		</br>
        <div id="cajaerror" class="alerterror">
            <span id="errorconsulta">ERROR:</span>
            </br>
        </div>
  			</br>
        <div class="text-center">
        		<button type="button" class="btn btn-secondary btn-alb" id="btnFinConsulta" on-click="finConsulta">Cerrar</button>
        </div>
      </div>

    `;
  }

  static get properties() {
    return {
      datos: {
        type: Object
      }, lanzador: {
        type: String,
        observer: "_cambioDatos"
      }
    };
  }

  _cambioDatos() {
    console.log("Mostrando datos");

    this.$.nombre.value = this.datos.nombre;
    this.$.apellido.value = this.datos.apellido;
    this.$.domicilio.value = this.datos.domicilio;
    this.$.provincia.value = this.datos.provincia;
    this.$.codpostal.value = this.datos.codpostal;
    this.$.nif.value = this.datos.nif;
    this.$.telefono.value = this.datos.telefono;
  }

  finConsulta() {

    console.log("Cerrar pulsado");

    this.generaEvento("finConsulta");
  }

  generaEvento(evento) {

    this.dispatchEvent(
      new CustomEvent(
        "eventconsulta",
        {
          detail: {
            "evento": evento
          }
        }
      )
    )
  }
}

window.customElements.define('consulta-usuario', ConsultaUsuario);
